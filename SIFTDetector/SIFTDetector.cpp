/*
 * SIFTDetector.cpp
 *
 *  Created on: Aug 25, 2017
 *      Author: baohq
 */

#include "SIFTDetector.h"

using namespace std;
using namespace cv;
using namespace xfeatures2d;
using namespace boost::filesystem;

SIFTDetector::SIFTDetector()
{

}

SIFTDetector::SIFTDetector(CameraController *camCtrl, string imgspath) : BaseObjectDetector("SIFT.csv")
{
    cameraControler = camCtrl;
    trainingImagePath = imgspath;
    featureDetector = SiftFeatureDetector::create();
    featureExtractor = SiftDescriptorExtractor::create();
}

void SIFTDetector::trainModel()
{
    Mat trainingRgbImg = imread(trainingImagePath), trainingGrayImg;

    //create gray color image
    cvtColor(trainingRgbImg, trainingGrayImg, CV_RGB2GRAY);

    //detect key-points of gray color training image
    featureDetector->detect(trainingGrayImg, trainKp);

    //compute descriptor of gray color training image
    featureExtractor->compute(trainingGrayImg, trainKp, trainDesc);

    //add and train descriptor of key-points of training image to FLANN matcher

    flannMatcher.add(trainDesc);
}

void SIFTDetector::startBFDetect()
{
    //train key-points descriptor from training image
    trainModel();

    //get image from camera and detect matches
    while(char(waitKey(1)) != 'q')
    {
        double t0 = getTickCount();		//for calculate frame rate
        Mat testRgbImg, testGImg;		//test image

        //get new image frame from camera
//        testRgbImg = cameraControler->getNewFrame();
        testRgbImg = imread("afc3.jpg");
        cvtColor(testRgbImg, testGImg, CV_RGB2GRAY);
        vector<KeyPoint> testKp;		//test image key-points
        Mat testDesc;					//test image kp descriptor

        //detect key-points of test image
        featureDetector->detect(testGImg, testKp);

        //compute kp descriptor of test image
        featureExtractor->compute(testGImg, testKp, testDesc);

        //matches key-points between test image and train image
        vector< vector<DMatch> > matches;

        //find matching points
        bfMatcher.knnMatch(testDesc, matches, 2);

        //filter for good matches
        vector<DMatch> goodMatches;
        for(int i = 0; i < matches.size(); i++)
        {
            if(matches[i][0].distance < 0.8 * matches[i][1].distance)
            {
                goodMatches.push_back(matches[i][0]);
            }
        }

        //draw result and show
        Mat imgShow, trainImg = imread(trainingImagePath);
        drawMatches(testRgbImg, testKp, trainImg, trainKp, goodMatches, imgShow);
        imshow("result", imgShow);

        cout << "Frame rate = " << getTickFrequency() / (getTickCount() - t0) << endl;

    }
}

void SIFTDetector::startDetecting()
{
    //train key-points descriptor from training image
    trainModel();

    //get image from camera and detect matches
    while(char(waitKey(1)) != 'q')
    {
        double t0 = getTickCount();		//for calculate frame rate
        Mat testRgbImg, testGImg;		//test image

        //get new image frame from camera
        testRgbImg = cameraControler->getNewFrame();
        cvtColor(testRgbImg, testGImg, CV_RGB2GRAY);
        vector<KeyPoint> testKp;		//test image key-points
        Mat testDesc;					//test image kp descriptor

        //detect key-points of test image
        featureDetector->detect(testGImg, testKp);

        //compute kp descriptor of test image
        featureExtractor->compute(testGImg, testKp, testDesc);

        //matches key-points between test image and train image
        vector<vector<DMatch> > matches;

        //find matching points
        flannMatcher.knnMatch(testDesc, matches, 2);

        //filter for good matches
        vector<DMatch> goodMatches;
        vector<KeyPoint> matched1, matched2;
        for(int i = 0; i < matches.size(); i++)
        {
            if(matches[i][0].distance < 0.6 * matches[i][1].distance)
            {
                goodMatches.push_back(matches[i][0]);
                matched1.push_back(trainKp[matches[i][0].trainIdx]);
                matched2.push_back(testKp[matches[i][0].queryIdx]);
            }
        }

        Mat inlierMask, homography;
        vector<KeyPoint> inliers1, inliers2;
        vector<DMatch> inlierMatches;

        if (matched1.size() >= 4)
        {
            homography = findHomography(keyToPoints(matched1), keyToPoints(matched2), RANSAC, 2.5f, inlierMask);
        }

        if (matched1.size() < 4 || homography.empty())
        {
            continue;
        }

        for (unsigned i = 0; i < matched1.size(); i++)
        {
            if (inlierMask.at<uchar>(i))
            {
                int newI = static_cast<int>(inliers1.size());
                inliers1.push_back(matched1[i]);
                inliers2.push_back(matched2[i]);
                inlierMatches.push_back(DMatch(newI, newI, 0));
            }
        }

        cout << "Frame rate = " << getTickFrequency() / (getTickCount() - t0) << endl;
        cout << "Matches = " << inlierMatches.size() << endl;

        Mat imgShow, trainingRgbImg = imread(trainingImagePath);
        drawMatches(trainingRgbImg, inliers1, testRgbImg, inliers2, inlierMatches, imgShow);

        // Uncomment the line below to draw good matches only
        //drawMatches(testRgbImg, testKp, trainingRgbImg, trainKp, goodMatches, imgShow);

        //-- Get the corners from the image_1 ( the object to be "detected" )
        std::vector<Point2f> obj_corners(4);
        obj_corners[0] = cvPoint(0, 0);
        obj_corners[1] = cvPoint(trainingRgbImg.cols, 0);
        obj_corners[2] = cvPoint(trainingRgbImg.cols, trainingRgbImg.rows);
        obj_corners[3] = cvPoint(0, trainingRgbImg.rows);

        std::vector<Point2f> scene_corners(4);

        perspectiveTransform(obj_corners, scene_corners, homography);

        //-- Draw lines between the corners (the mapped object in the scene - image_2 )
        Point2f offset( trainingRgbImg.cols, 0);
        line( imgShow, scene_corners[0] + offset, scene_corners[1] + offset, Scalar(0, 255, 0), 4 );
        line( imgShow, scene_corners[1] + offset, scene_corners[2] + offset, Scalar( 0, 255, 0), 4 );
        line( imgShow, scene_corners[2] + offset, scene_corners[3] + offset, Scalar( 0, 255, 0), 4 );
        line( imgShow, scene_corners[3] + offset, scene_corners[0] + offset, Scalar( 0, 255, 0), 4 );

        resize(imgShow, imgShow, Size(1080, 720));
        imshow("SIFT", imgShow);
    }
}
