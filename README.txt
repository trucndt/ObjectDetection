Dependencies:
  - OpenCV
  - Boost

Directory structure:
  - SVMClassifier/ : source code for image classifier with SVM
  - ObjectTracking : source code for running the object tracking application
  - ORBDetector/ : source code for ORB detector
  - SIFTDetector/ : source code for SIFT detector
  - SURFDetector/ : source code for SURF detector
  - BRISKDetector/ : source code for BRISK detector
  - Mediator/ : source code for Mediator

To compile SVMClassifier:
  + Execute these commands:
    $ cd ObjectDetection
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make SVMClassifier

To run SVMClassifier
  + Make sure that the binary file (SVMClassifier), the folder model/ and vocab.xml resides in the same dir
  + Execute these commands:
    $ ./SVMClassifier 2> /dev/null

  + By default, the program runs from images in the test folder "../data/test" (can be re-specified by --test), for example:
    $ ./SVMClassifier --test <path/to/test/folder> 2> /dev/null

  + Otherwise, to run from video, execute as follows:
    $ ./SVMClassifier --video <path/to/video/file> 2> /dev/null

To compile ObjectTracking and Mediator (need to cross-compile to run on RPi)
  + Execute these commands:
    $ cd ObjectDetection
    $ mkdir build
    $ cd build
    $ cmake -DCMAKE_TOOLCHAIN_FILE=../Toolchain-RaspberryPi.cmake ..
    $ make

To run ObjectTracking and Mediator:
  + Run ObjectTracking on RPi 2, make sure that the binary file (ObjectTracking), the folder model/ and vocab.xml resides in the same dir
    $ ./ObjectTracking 2> /dev/null

  + Run Mediator on RPi 1:
    $ ./Mediator

