/*
 * CameraController.h
 *
 * Created on: Mar 3, 2017
 * Author: TrucNDT
*/

#ifndef CAMERACONTROLLER_H
#define CAMERACONTROLLER_H

#include <opencv2/opencv.hpp>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <signal.h>

class CameraController
{
public:
    CameraController();
    virtual ~CameraController();

    int enableCamera();
    cv::Mat getNewFrame();

    void startCamera();

    unsigned long totalFrames;

private:
    cv::VideoCapture videoCapture;
    cv::Mat newFrame;
    boost::mutex mtxNewFrame;
    boost::condition_variable condNewFrame;

    boost::thread *mCam;
    bool isNewFrame;

    void updateFrame();
};

#endif // CAMERACONTROLLER_H
