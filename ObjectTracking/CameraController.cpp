/*
 * CameraController.cpp
 *
 * Created on: Mar 3, 2017
 * Author: TrucNDT
*/

#include "CameraController.h"
#include <Mediator/utils.h>

using namespace cv;
using namespace std;

CameraController::CameraController()
{
    isNewFrame = false;
    totalFrames = 0;
}

int CameraController::enableCamera()
{
    videoCapture.open(0);
    videoCapture.set(CV_CAP_PROP_FRAME_WIDTH, 640);
    videoCapture.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
    //Check if camera is open
    if (!videoCapture.isOpened())
    {
        cout << "Fail to open camera\n";
        return -1;
    }
    cout << "Open camera successfully.\n";
    return 0;
}

void CameraController::updateFrame()
{
    pthread_setname_np(pthread_self(), "update");
    makeRealTimeThread();
    isNewFrame = false;

    while(true)
    {
        videoCapture >> newFrame;

        if (newFrame.empty())
        {
            continue;
        }

        totalFrames++;
        boost::unique_lock<boost::mutex> lock(mtxNewFrame);
        isNewFrame = true;
        lock.unlock();
        condNewFrame.notify_all();
    }
}

Mat CameraController::getNewFrame()
{
    boost::unique_lock<boost::mutex> lock(mtxNewFrame);

    while (!isNewFrame)
    {
        condNewFrame.wait(lock);
    }

    isNewFrame = false;

    return newFrame.clone();
}

void CameraController::startCamera()
{
    mCam = new boost::thread(boost::bind(&CameraController::updateFrame, this));
}

CameraController::~CameraController()
{
    pthread_kill(mCam->native_handle(), 9);
    videoCapture.release();
    delete mCam;
}
