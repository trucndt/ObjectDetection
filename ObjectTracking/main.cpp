#include "BaseObjectDetector.h"
#include "CameraController.h"
#include "SpokesmanPi2.h"
#include <SVMClassifier/SVMCategorizer.h>

using namespace cv;
using namespace std;
using namespace boost::program_options;

variables_map gPROG_ARGUMENT;
int processCommandLineArgument(int argc, char **argv);

int main(int argc, char *argv[])
{
    if (processCommandLineArgument(argc, argv) < 0)
    {
        return 0;
    }

    cout << "Built time: " << __DATE__ << " " << __TIME__ << endl;

    CameraController *cam = new CameraController();
    BaseObjectDetector *svm  = new SVMCategorizer(gPROG_ARGUMENT["clusters"].as<int>(), cam,
                                                  String(), gPROG_ARGUMENT["svmfiles"].as<string>());

    //start camera
    cam->enableCamera();
    cam->startCamera();

    svm->trainModel();

    //start receive message from Pi1 for detection
    makeRealTimeThread();

    //save output video
    svm->trackingCamera(true);
//    SpokesmanPi2 spokesman(svm);
//    spokesman.initialize();
//    spokesman.listen();

	delete cam;
    delete svm;

    return 0;
}

int processCommandLineArgument(int argc, char **argv)
{
    options_description usage("Usage");

    usage.add_options()
        ("help,h", "Print help message")
        ("svmfiles,f", value<string>()->default_value("model"), "Model directory")
        ("clusters,c", value<int>()->default_value(300), "Number of words in the bag of words");

    try
    {
        store(command_line_parser(argc, argv).options(usage).run(), gPROG_ARGUMENT);

        if (gPROG_ARGUMENT.count("help"))
        {
            cout << usage << endl;
            return -1;
        }

        notify(gPROG_ARGUMENT);
    }
    catch (required_option& e)
    {
        cout << "ERROR: " << e.what() << endl;
        cout << usage << endl;
        return -1;
    }
    catch (boost::program_options::error &e)
    {
        cout << "ERROR: " << e.what() << endl;
        return -1;
    }

    return 0;
}
