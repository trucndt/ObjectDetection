//
// Created by trucnguyen on 9/7/17.
//

#ifndef OBJECTDETECTION_COLLECTRESULT_H
#define OBJECTDETECTION_COLLECTRESULT_H

#include <stdio.h>

/**
 * Write result to file, csv format
 */
class CollectResult
{
public:
    /**
     * Constructor
     * @param filename Name of result file
     */
    CollectResult(const char* filename);
    virtual ~CollectResult();

    /**
     * Record frame rate to file
     * @param fps frame per second
     */
    void recordFps(double fps);

    /**
     * Record numOfMatches to file
     * @param numOfMatches number of matches
     */
    void recordMatches(int numOfMatches);

    /**
     * Check if enough run times
     * @return true if enough
     */
    bool enoughTimes();

private:
    /// Max number of samples
    static const int MAX_SAMPLES = 200;

    /// Result file pointer
    FILE *mOut;

    /// Count number of runs
    int mCount;
};


#endif //OBJECTDETECTION_COLLECTRESULT_H
