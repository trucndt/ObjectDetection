//
// Created by trucnguyen on 9/7/17.
//

#include "BaseObjectDetector.h"

using namespace std;
using namespace cv;

BaseObjectDetector::~BaseObjectDetector()
{
//    delete mCollect;
}

BaseObjectDetector::BaseObjectDetector()
{
    mMidPoint = Point2f(-1, -1);

#ifdef CROSSCOMPILE
    spokesmanPi2 = new SpokesmanPi2;
    spokesmanPi2->initialize();
//    mIsNewMidPoint = false;
#endif
}

BaseObjectDetector::BaseObjectDetector(const char *resultFileName) : BaseObjectDetector()
{
//    mCollect = new CollectResult(resultFileName);
}

cv::Point2f BaseObjectDetector::goodBoundingBoxChecking(const vector<Point2f> &coors, double degreesTH)
{
    Point2f result(-1.0, -1.0);

    //store coefficients of lines: ax + by = c
    //AB - _Coeffs[0]
    //BC - _Coeffs[1]
    //CD - _Coeffs[2]
    //DA - _Coeffs[3]
    vector<double> aCoeffs;
    vector<double> bCoeffs;
    vector<double> cCoeffs;

    //Check if quadrilateral ABCD is a good bounding box
    //A is coors[0]
    //B is coors[1]
    //C is coors[2]
    //D is coors[3]

    for(int i = 0; i < coors.size(); i++)
    {
        //Calculate AB/BC/CD/DA equations: y = slope*x + intercept (y = ax + b)
        double abSlope = (coors[i].y - coors[(i + 1) % coors.size()].y) /
            (coors[i].x - coors[(i + 1) % coors.size()].x);
        double abIntercept = coors[i].y - abSlope * coors[i].x;

        //convert to ax + by = c
        aCoeffs.push_back(-abSlope);
        bCoeffs.push_back(1);
        cCoeffs.push_back(abIntercept);
    }

    //calculate length of AB, BC, AC, CD, AD
    double AB = calcDistance(coors[0], coors[1]);
    double AD = calcDistance(coors[3], coors[0]);
    double BC = calcDistance(coors[1], coors[2]);
    double AC = calcDistance(coors[0], coors[2]);
    double CD = calcDistance(coors[2], coors[3]);

    //AB cut CD or AD cut BC case
    //solve system equations AB and CD

    //determinants for AB and CD
    double ab_cdFixedDeterminant = (aCoeffs[0] * bCoeffs[2]) - (aCoeffs[2] * bCoeffs[0]);
    double ab_cdXDeterminant = (cCoeffs[0] * bCoeffs[2]) - (cCoeffs[2] * bCoeffs[0]);
    double ab_cdYDeterminant = (cCoeffs[2] * aCoeffs[0]) - (cCoeffs[0] * aCoeffs[2]);

    //determinants for AD and BC
    double ad_bcFixedDeterminant = (aCoeffs[1] * bCoeffs[3]) - (aCoeffs[3] * bCoeffs[1]);
    double ad_bcXDeterminant = (cCoeffs[1] * bCoeffs[3]) - (cCoeffs[3] * bCoeffs[1]);
    double ad_bcYDeterminant = (cCoeffs[3] * aCoeffs[1]) - (cCoeffs[1] * aCoeffs[3]);

    if(ab_cdFixedDeterminant == 0 || ad_bcFixedDeterminant == 0)
    {
        cout << "Fixed determinant is equal to zero.\n";
    }

    else
    {
        //intersection point of AB and CD
        Point2f ab_cd(ab_cdXDeterminant / ab_cdFixedDeterminant, ab_cdYDeterminant / ab_cdFixedDeterminant);

        //intersection point of AD and BC
        Point2f ad_bc(ad_bcXDeterminant / ad_bcFixedDeterminant, ad_bcYDeterminant / ad_bcFixedDeterminant);

        //check if intersection points between two points of AB/CD, AD/BC

        //intersection point to A (of AB line) distance
        double toAOfAB_Distance = calcDistance(ab_cd, coors[0]);

        //cutting point to B (of AB line) distance
        double toBOfAB_Distance = calcDistance(ab_cd, coors[1]);

        //cutting point to C (of CD line) distance
        double toCOfCD_Distance = calcDistance(ab_cd, coors[2]);

        //cutting point to D (of CD line) distance
        double toDOfCD_Distance = calcDistance(ab_cd, coors[3]);

        //cutting point to A (of AD line) distance
        double toAOfAD_Distance = calcDistance(ad_bc, coors[0]);

        //cutting point to D (of AD line) distance
        double toDOfAD_Distance = calcDistance(ad_bc, coors[3]);

        //cutting point to B (of BC line) distance
        double toBOfBC_Distance = calcDistance(ad_bc, coors[1]);

        //cutting point to C (of BC line) distance
        double toCOfBC_Distance = calcDistance(ad_bc, coors[2]);

        // Check for non-convex
        if(compareFloating((toAOfAB_Distance + toBOfAB_Distance), AB) <= 0)
        {
            cout << "Non-convex quadrilateral.\n";
            return result;
        }
        else if(compareFloating((toCOfCD_Distance + toDOfCD_Distance), CD) <= 0)
        {
            cout << "Non-convex quadrilateral.\n";
            return result;
        }
        else if(compareFloating((toAOfAD_Distance + toDOfAD_Distance), AD) <= 0)
        {
            cout << "Non-convex quadrilateral.\n";
            return result;
        }
        else if(compareFloating((toBOfBC_Distance + toCOfBC_Distance), BC) <= 0)
        {
            cout << "Non-convex quadrilateral.\n";
            return result;
        }
    }

    //angle of quadrilateral is smaller than 30.0 degrees case
    //cosines rule a^2 + b^2 - 2abcos(C) = c^2
    //split ABCD into: ABC and ACD

    //angle between AB and AC (angle A of ABC)
    double abc_BAC = acos((AB*AB + AC*AC - BC*BC) / (2*AB*AC));

    //angle between BA and BC (angle B of ABC)
    double abc_ABC = acos((AB*AB + BC*BC - AC*AC) / (2*AB*BC));

    //angle between CA and BC (angle C of ABC)
    double abc_ACB = acos((AC*AC + BC*BC - AB*AB) / (2*AC*BC));

    //angle between AD and AC (angle A of ACD)
    double adc_DAC = acos((AD*AD + AC*AC - CD*CD) / (2*AD*AC));

    //angle between DA and DC (angle D of ACD)
    double adc_ADC = acos((AD*AD + CD*CD - AC*AC) / (2*AD*CD));

    //angle between CA and CD (angle C of ACD)
    double adc_ACD = acos((AC*AC + CD*CD - AD*AD) / (2*AC*CD));

    //final A/B/C/D angles
    double A = (abc_BAC + adc_DAC) * 180.0 / M_PI;
    double B = abc_ABC * 180.0 / M_PI;
    double C = (abc_ACB + adc_ACD) * 180.0 / M_PI;
    double D = adc_ADC * 180.0 / M_PI;

    if (compareFloating(A, degreesTH) < 0 || compareFloating(B, degreesTH) < 0 ||
        compareFloating(C, degreesTH) < 0 || compareFloating(D, degreesTH) < 0)
    {
        cout << "Angle smaller than 30.0 degrees.\n";
        return result;
    }

    //calculate AC, BD equation
    //AC
    double ac_Slope = (coors[0].y - coors[2].y) / (coors[0].x - coors[2].x);
    double ac_Intercept = coors[0].y - ac_Slope * coors[0].x;
    double ac_aCoeff = -ac_Slope;
    double ac_bCoeff = 1;
    double ac_cCoeff = ac_Intercept;

    //BD
    double bd_Slope = (coors[1].y - coors[3].y) / (coors[1].x - coors[3].x);
    double bd_Intercept = coors[1].y - bd_Slope * coors[1].x;
    double bd_aCoeff = -bd_Slope;
    double bd_bCoeff = 1;
    double bd_cCoeff = bd_Intercept;

    //find solution of AC and BD
    double ac_bdFixedDeterminant = ac_aCoeff * bd_bCoeff - bd_aCoeff * bd_bCoeff;
    double ac_bdXDeterminant = ac_cCoeff * bd_bCoeff - bd_cCoeff * ac_bCoeff;
    double ac_bdYDeterminant = ac_aCoeff * bd_cCoeff - bd_aCoeff * ac_cCoeff;

    result.x = ac_bdXDeterminant /  ac_bdFixedDeterminant;
    result.y = ac_bdYDeterminant /  ac_bdFixedDeterminant;

    return result;
}

Point2f BaseObjectDetector::getMidPoint()
{
//    boost::unique_lock<boost::mutex> lock(mMtx_MidPoint);
//
//    while (mIsNewMidPoint == false)
//    {
//        mCond_MidPoint.wait(lock);
//    }
//
//    mIsNewMidPoint = false;

    startDetecting();

    return mMidPoint;
}

void BaseObjectDetector::updateMidPoint(const cv::Point2f &midPoint)
{
//    boost::unique_lock<boost::mutex> lock(mMtx_MidPoint);
    mMidPoint.x = midPoint.x;
    mMidPoint.y = midPoint.y;
//    mIsNewMidPoint = true;
//    lock.unlock();
//    mCond_MidPoint.notify_all();
}

