//
// Created by trucnguyen on 9/7/17.
//

#ifndef OBJECTDETECTION_BASEOBJECTDETECTOR_H
#define OBJECTDETECTION_BASEOBJECTDETECTOR_H

#include "CollectResult.h"
#include <Mediator/utils.h>
#include <boost/thread.hpp>
#include <boost/program_options.hpp>
#include "SpokesmanPi2.h"

extern boost::program_options::variables_map gPROG_ARGUMENT;
class SpokesmanPi2;

/**
 * Base class for Object Detection algorithms
 */
class BaseObjectDetector
{
public:
    /**
     * Default initialization
     */
    BaseObjectDetector();

    /**
     * Constructor with new CollectResult object
     * @param resultFileName name of the result file
     */
    BaseObjectDetector(const char* resultFileName);

    virtual ~BaseObjectDetector();

    /**
     * Virtual method for the FLANN algorithm
     */
    virtual void startDetecting() {};

    /**
     * Virtual method for training descriptors
     */
    virtual void trainModel() {};

    virtual void startStaticDetecting(const std::string &testFolder, int numOfRegions) {};

    virtual void startVideoDetecting(const std::string &videoPath, int numOfRegions) {};

    virtual void trackingVideo() {};
    virtual void trackingCamera(bool) {};

    /**
     * Getter for mMidPoint, use condition variable to wait for the updated mMidPoint
     * @return the mMidPoint
     */
    cv::Point2f getMidPoint();

protected:

    /**
     * Check if the bounding box is a good one
     * @param coors The 4 points of the bounding box
     * @param degreesTH The minimum value of angles
     * @return (-1, -1) if a bad one, otherwise, return the middle point of the bounding box
     */
    static cv::Point2f goodBoundingBoxChecking(const std::vector<cv::Point2f> &coors, double degreesTH = 30);

    void updateMidPoint(const cv::Point2f &midPoint);

    /// object of CollectResult
    CollectResult *mCollect;

    /// Store the middle point (intersection of two diagonals) of the bounding box
    /// = (-1, -1) if bad bounding box or non-detected
    cv::Point2f mMidPoint;

    SpokesmanPi2 *spokesmanPi2;

//  Unused for now
//    /// true if the mMidPoint is up-to-date, used for condition variable
//    bool mIsNewMidPoint;
//
//    /// Mutex for mMidPoint
//    boost::mutex mMtx_MidPoint;
//
//    /// Condition variable for mMidPoint
//    boost::condition_variable mCond_MidPoint;
};


#endif //OBJECTDETECTION_BASEOBJECTDETECTOR_H
