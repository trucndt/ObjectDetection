//
// Created by trucnguyen on 9/7/17.
//

#include "CollectResult.h"


CollectResult::CollectResult(const char *filename)
{
    mOut = fopen(filename, "w");
    mCount = 0;
}

CollectResult::~CollectResult()
{
    fclose(mOut);
}

void CollectResult::recordFps(double fps)
{
    fprintf(mOut, "FrameRate,%lf\n", fps);
}

void CollectResult::recordMatches(int numOfMatches)
{
    fprintf(mOut, "Matches,%d\n", numOfMatches);
}

bool CollectResult::enoughTimes()
{
    mCount++;
    if (mCount == MAX_SAMPLES)
        return true;
    else return false;
}
