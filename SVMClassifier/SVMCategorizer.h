/*
 * SVMClassifier.h
 *
 *  Created on: Aug 26, 2017
 *      Author: baohq
 */

#ifndef OBJECTCLASSIFIER_SVMCATEGORIZER_H_
#define OBJECTCLASSIFIER_SVMCATEGORIZER_H_

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/ximgproc/segmentation.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/tracking.hpp>
#include "opencv2/core/utility.hpp"
#include "../ObjectTracking/CameraController.h"
#include <boost/filesystem.hpp>
#include <string>
#include <set>
#include <map>
#include <iterator>
#include <algorithm>
#include <functional>
#include "../ObjectTracking/BaseObjectDetector.h"
#include "../ObjectTracking/SpokesmanPi2.h"

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/map.hpp>

class CameraController;
class SpokesmanPi2;
extern boost::program_options::variables_map gPROG_ARGUMENT;

class SVMCategorizer : public BaseObjectDetector {
public:

	//create list of directories
	friend int createDirectories(const std::string &, const std::vector<std::string> &);

	SVMCategorizer();

	/**
	 * @param clusters number of clusters for bag-of-word to cluster
	 * @param cam for camera controller
	 * @param tempFolder path to template folder
	 * @param trainFolder path to training folder
	 */
	SVMCategorizer(int clusters, CameraController *cam, const std::string &trainFolder, const std::string &modelFolder);

	/**
	 * create bag of words
	 */
	void buildVocab();

	/**
	 * train svm models
	 */
	virtual void trainClassifier();

	void trainModel();

	/**
	 * @brief Classify + localize object for saved images
	 * @param testFolder path to test folder
	 * @param numOfRegions number of regions to apply object classifier
	 */
	void startStaticDetecting(const std::string &testFolder, int numOfRegions);

    /**
     * Detect frames from video
     * @param videoPath path to video file
     */
    void startVideoDetecting(const std::string &videoPath, int numOfRegions);

	/**
	 * @brief real-time object detection for frame from camera
	 */
	void startDetecting(int numOfRegions);
	void startDetecting();
	void trackingVideo();
	void trackingCamera(bool storeVideo=false);

	/**
	 * @brief load trained svm and bag of words models
	 * @param pathToModelDir path to svm models directory
	 */
	void loadSVMModel(const std::string &pathToModelDir);

protected:
	CameraController *cameraController;			//to get frame from camera
	std::string trainFolder;					//path to folder contains training images
	std::string modelFolder;					//path to folder contains svm models

	/*
	 * gTemplates a list of training images
	 * positiveData maps a category name to positive data of the category
	 * negativeData maps a category name to negative data of the category
	 */
	std::vector<cv::Mat> gTemplates;
	std::map<std::string, cv::Mat> positiveData, negativeData;
	std::multimap<std::string, cv::Mat> trainingSet;		//maps category name to its training images data
	std::map<std::string, cv::Ptr<cv::ml::SVM> > svms;		//SVM model for categories - multi-class svm
	std::vector<std::string> categoryNames;
	cv::Mat vocabulary;		//contains N clusters of image descriptors
	int categories;			//number of categories
	int clusters;			//number of cluster

    /// Store parameters of sigmoid functions
    std::map<std::string, std::pair<double, double> > mSigmoidFunction;

	cv::Ptr<cv::FeatureDetector> featureDetector;		//to detect key-points from image
	cv::Ptr<cv::DescriptorExtractor> descriptorExtractor;	//to compute key-points descriptor
	cv::Ptr<cv::BOWKMeansTrainer> bowTrainer;		//bag of words trainer
	cv::Ptr<cv::BOWImgDescriptorExtractor> bowDescriptorExtractor;	//to detect and compute
	cv::Ptr<cv::FlannBasedMatcher> flannMatcher;	//to search key-points in an image and match them to training data
	cv::Ptr<cv::ximgproc::segmentation::SelectiveSearchSegmentation> sssObj; //for selective search segmentation method
	cv::Ptr<cv::Tracker> tracker;

    /// Check for traced box in findMaxOverlappedBox()
    bool trace[100];

	/**
	 * create training set
	 */
	void makeTrainingSet();

	/**
	 * create positive and negative data
	 */
	void makePosNegData();

	/**
	 *
	 * @param fullName full path of a file
	 * @return file name without extension
	 */
	std::string removeExt(const std::string &fullName);

	/**
	 * @brief overlapping bounding boxes suppression
	 * @param boxes list of boxes score object positive
	 * @param threshold	overlapping area threshold
	 * @return list of picked boxes
	 */
	std::vector<cv::Rect> boxNMS(const std::vector<cv::Rect> &boxes, double threshold);

	/**
	 * @brief check if the overlapping area is greater than threshold
	 * @param toCheckRect box will be checked
	 * @param oriRect considering box
	 * @param threshold	specific threshold - recommend 0.3 to 0.5
	 * @return true if the overlapping area is greater than threshold
	 */
	int checkOverlap(const cv::Rect &toCheckRect, const cv::Rect &oriRect, double threshold);

	/**
	 * @brief transforms size of object proposals bounding boxes from
	 * 320x240 frame to 640x480 frame
	 * @param oriRects 320x240 frame bounding boxes
	 * @param ratio raito of widths of two frames
	 * @return bounding boxes fit 640x480 frame
	 */
	std::vector<cv::Rect> transformRect(const std::vector<cv::Rect> &oriRects, double ratio);

	/**
	 * @brief get re-define ROI of image (bottom 2/3 image)
	 * @param img original image
	 * @return sub-image
	 */
	cv::Mat getROI(const cv::Mat &img);

	/**
	 * @brief run object detection on input each frame
	 * @param frame raw frame to run object detection
	 * @param numOfRegions number of regions to apply object classifier
	 * @return successful or failed detection and processed frame (if successful)
	 */
	std::pair<std::string, cv::Mat> runDetection(cv::Mat frame, int numOfRegions, std::vector<cv::Rect> &rBoxes);

    /**
     * Train parameters for sigmoid function to determine probabilistic outputs
     * Update to mSigmoidFunction
     * @param cate name of category
     */
    void trainSigmoid(const std::string &cate);

    /**
     * Load mSigmoidFunction from file
     */
	void loadSigmoid();

    /**
     * Save mSigmoidFunction to file
     */
	void saveSigmoid();

    /**
     * A recursion function to find the list of overlapped box
     * @param index index of current box
     * @param bbox list of bounding boxes
     * @param D list of indices of overlapped boxes
     * @param T final list of list of overlapped boxes
     * @param rectD list of current overlapped boxes
     * @param valid valid of output
     */
    void findOverlapBoxes(int index,
                          const std::vector<cv::Rect> &bbox,
                          std::list<int> &D,
                          std::vector<std::list<cv::Rect> > &T,
                          std::list<cv::Rect> &rectD,
                          bool &valid);

    /**
     * Suppress bounding boxes by only keeping the overlapped sections of all boxes
     * @param bbox list of bounding boxes
     * @return list of overlapped boxes
     */
    std::vector<cv::Rect> findMaxOverlappedBox(const std::vector<cv::Rect> &bbox);

    /**
     * Find the overlapped section of two boxes
     * @param firstBox the first box
     * @param secondBox the second box
     * @return the overlapped section
     */
    cv::Rect overlapTwoBoxes(const cv::Rect& firstBox, const cv::Rect& secondBox);
};

/**
 * @brief for std::set comparator
 */
struct comparator
{
	bool operator()(const std::pair<double, std::vector<int>> &element1,
                    const std::pair<double, std::vector<int>> &element2)
	{
		return element1.first > element2.first;
	}
};

/**
 * @brief sort the vector according to their value and return their indices order
 * @param values
 * @return
 */
template<typename T>
std::vector<int> argSort(std::vector<T> const& values)
{
	std::vector<int> indices(values.size());
	std::iota(std::begin(indices), std::end(indices), static_cast<size_t>(0));
	std::sort(
			std::begin(indices), std::end(indices),
			[&](int a, int b) { return values[a] > values[b]; }
	);

	return indices;
}



#endif /* OBJECTCLASSIFIER_SVMCATEGORIZER_H_ */
