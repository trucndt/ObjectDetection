/*
 * SVMClassifier.cpp
 *
 *  Created on: Aug 26, 2017
 *      Author: baohq
 */

#include "SVMCategorizer.h"
#include <fstream>

using namespace std;
using namespace cv;
using namespace boost::filesystem;

SVMCategorizer::SVMCategorizer() : BaseObjectDetector() {
    // TODO Auto-generated constructor stub

}

/**
 *
 * @param clusters number of clusters for bag-of-word to cluster
 * @param cam for camera controller
 * @param tempFolder path to template folder
 * @param trainFolder path to training folder
 */
SVMCategorizer::SVMCategorizer(int clusters,
                               CameraController *cam,
                               const string &trainFolder,
                               const string &modelFolder)
{
    //Initialize attributes
    this->clusters = clusters;
    this->cameraController = cam;
    this->trainFolder = trainFolder;
    this->modelFolder = modelFolder;
    categories = 0;
    featureDetector = BRISK::create();
    descriptorExtractor = BRISK::create();
    bowTrainer = (new BOWKMeansTrainer(this->clusters));
    flannMatcher = new FlannBasedMatcher(new flann::LshIndexParams(20, 10, 2));
    bowDescriptorExtractor = new BOWImgDescriptorExtractor(descriptorExtractor, flannMatcher);
    sssObj = ximgproc::segmentation::createSelectiveSearchSegmentation();
    tracker = TrackerTLD::createTracker();
}

/**
 *
 * @param pPath path to folder
 * @param listOfNames list of folder name need to create
 * @return
 */
int createDirectories(const string &pPath, const vector<string> &listOfNames)
{
    for(int i = 0; i < listOfNames.size(); i++)
    {
        path dirPath(pPath + string("/") + listOfNames[i]);
        if(!exists(dirPath))
        {
            if(!create_directory(dirPath))
            {
                cout << "Error creating directory.\n";
                return 1;
            }
        }
    }

    return 0;
}

/**
 *
 * @param fullName full path of a file
 * @return file name without extension
 */
string SVMCategorizer::removeExt(const string &fullName)
{
    int fIndex = fullName.find_last_of(".");
    string name = fullName.substr(0, fIndex);

    return name;
}

/**
 * create training set
 */
void SVMCategorizer::makeTrainingSet()
{
    string category;
    recursive_directory_iterator endIter;

    for(recursive_directory_iterator iter(trainFolder); iter != endIter; ++iter)
    {
        //level 0 means a folder, since there are only folders in TRAIN_FOLDER at the zeroth level
        if(iter.level() == 0)
        {
            //get category name from name of the folder
            category = iter->path().filename().string();
        }

        //level 1 means a training image, map that by the current category
        else
        {
            //file name with path
            string fileName = trainFolder + string("/") + category + string("/") + iter->path().filename().string();

            //make a pair of string and Mat to insert into multimap
            pair<string, Mat> p(category, imread(fileName, CV_LOAD_IMAGE_GRAYSCALE));
            trainingSet.insert(p);
        }
    }
}

/**
 * create positive and negative data
 */
void SVMCategorizer::makePosNegData()
{
    //iterate through the whole training set of images
    for(multimap<string, Mat>::iterator iter = trainingSet.begin(); iter != trainingSet.end(); ++iter)
    {
        //category name is the first element of each entry in makeTrainingSet
        string category = iter->first;
        Mat gImg, fDesc;

        //training image is the second element
        gImg = iter->second;

        //detect key-points, get the image BOW descriptor
        vector<KeyPoint> kp;
        featureDetector->detect(gImg, kp);
//		if((kp.size() <= 3) && (iter->first.compare("background") != 0)) continue;
        bowDescriptorExtractor->compute(gImg, kp, fDesc);

        //mats to hold the positive and negative training data for current category
        for(int i = 0; i < categories; i++)
        {
            string checkCategory = categoryNames[i];

            //add BOW feature as positive sample for current category
            if(checkCategory.compare(category) == 0)
            {
                positiveData[checkCategory].push_back(fDesc);
            }

            //add BOW feature as negative sample for all other categories
            else
            {
                negativeData[checkCategory].push_back(fDesc);
            }
        }
    }

    //For debug
    for(int i = 0; i< categories; i++)
    {
        string category = categoryNames[i];
        cout << "Category " << category << ": " << positiveData[category].rows << " positives, "
                << negativeData[category].rows << " negatives" << endl;
    }
}

/**
 * create bag of words
 */
void SVMCategorizer::buildVocab()
{
    string category;
    recursive_directory_iterator endIter;

    for(recursive_directory_iterator iter(trainFolder); iter != endIter; ++iter)
    {
        //level 0 means a folder, since there are only folders in TRAIN_FOLDER at the zeroth level
        if(iter.level() == 0)
        {
            //get category name from name of the folder
            category = iter->path().filename().string();
        }

        //level 1 means a training image, map that by the current category
        else
        {
            //file name with path
            string fileName = trainFolder + string("/") + category + string("/") + iter->path().filename().string();

            //make a pair of string and Mat to insert into multimap
            Mat gImg = imread(fileName, CV_LOAD_IMAGE_GRAYSCALE);
            resize(gImg, gImg, Size(640, 480));
            gTemplates.push_back(gImg);
        }
    }

    //mat to hold BRISK descriptors for all templates
    Mat vocabDesc;


    //for each template, extract BRISK descriptors and pool them into vocab_descriptors
    for(int j = 0; j < gTemplates.size(); j++)
    {
        vector<KeyPoint> kp;
        Mat desc;
        featureDetector->detect(gTemplates[j], kp);

        descriptorExtractor->compute(gTemplates[j], kp, desc);
        vocabDesc.push_back(desc);
    }

    vocabDesc.convertTo(vocabDesc, CV_32F);
    //add the descriptors to the BOW trainer to cluster
    bowTrainer->add(vocabDesc);

    //cluster the BRISK descriptors
    vocabulary = bowTrainer->cluster();

    vocabulary.convertTo(vocabulary, CV_8U);

    //save the vocabulary
    FileStorage fs("vocab.xml", FileStorage::WRITE);
    fs << "vocabulary" << vocabulary;
    fs.release();

    cout << "Built vocabulary.\n";
}

/**
 * train svm models
 */
void SVMCategorizer::trainClassifier()
{
    cout << "Start training classifier.\n";

    //set the vocabulary for the BOW descriptor extractor
    bowDescriptorExtractor->setVocabulary(vocabulary);

    //extract BOW descriptors for all training images and organize them into positive and negative samples for each category
    makePosNegData();

    for(int i = 0; i < categories; i++) {
        const string &category = categoryNames[i];
        if (category == "background") continue;

        Mat trainData = positiveData[category];

        //positive training data has labels 1
        Mat trainLabel = Mat::ones(trainData.rows, 1, CV_32S);
        trainData.push_back(negativeData[category]);

        //negative training data has labels 0
        Mat m(negativeData[category].rows, 1, CV_32S, -1);
        trainLabel.push_back(m);

        //train SVM
        Ptr<ml::TrainData> train = ml::TrainData::create(trainData, ml::ROW_SAMPLE, trainLabel);

        svms[category]->trainAuto(train, 10,
                                  ml::SVM::getDefaultGrid(ml::SVM::C),
                                  ml::SVM::getDefaultGrid(ml::SVM::GAMMA),
                                  ml::SVM::getDefaultGrid(ml::SVM::P),
                                  ml::SVM::getDefaultGrid(ml::SVM::NU),
                                  ml::SVM::getDefaultGrid(ml::SVM::COEF),
                                  ml::SVM::getDefaultGrid(ml::SVM::DEGREE),
                                  true);

        //save SVM to file for possible reuse
        string svmFileName = string(string("model/") + category + string("SVM.xml"));
        svms[category]->save(svmFileName);

        // Train sigmoid param
        trainSigmoid(category);

        cout << "Trained and saved SVM for category " << category << endl;
    }

    // Write sigmoid param to file
    saveSigmoid();
}

void SVMCategorizer::trainSigmoid(const string &cate)
{
    int prior1 = positiveData[cate].rows;
    int prior0 = negativeData[cate].rows;
    cout << "Positive size: " << positiveData[cate].rows << endl;
    cout << "Negative size: " << negativeData[cate].rows << endl;

    int len = prior0 + prior1;
    double hiTarget = (prior1 + 1.0) / (prior1 + 2.0);
    double loTarget = 1 / (prior0 + 2.0);

    vector<double> deci(len, 0);
    vector<double> t(len, loTarget);

    const int maxiter = 100;
    const double minstep = 1e-10;
    const double sigma = 1e-12;
    double eps=1e-5;
    double fApB,p,q,h11,h22,h21,g1,g2,det,dA,dB,gd,stepsize;
    double newA,newB,newf,d1,d2;

    for (int i = 0; i < len; i++)
    {
        Mat desc;
        if (i < prior1) desc = positiveData[cate].row(i);
        else desc = negativeData[cate].row(i - prior1);

        deci[i] = svms[cate]->predict(desc, noArray(), true);
    }

    for (int i = 0; i < prior1; i++)
    {
        t[i] = hiTarget;
    }

    double A = 0.0, B = log((prior0 + 1.0) / (prior1 + 1.0)), fval = 0.0;
    for (int i = 0; i < len; i++)
    {
        fApB = deci[i] * A + B;
        if (fApB >= 0)
            fval += t[i] * fApB + log(1 + exp(-fApB));
        else
            fval += (t[i] - 1) * fApB + log (1 + exp(fApB));
    }

    int it;
    for (it = 0; it < maxiter; it++)
    {
        h11 = sigma, h22 = sigma, h21 = 0.0, g1 = 0.0, g2 = 0.0;
        for (int i = 0; i < len; i++)
        {
            fApB = deci[i] * A + B;
            if (fApB >= 0)
            {
                p=exp(-fApB)/(1.0+exp(-fApB));
                q=1.0/(1.0+exp(-fApB));
            }
            else
            {
                p=1.0/(1.0+exp(fApB));
                q=exp(fApB)/(1.0+exp(fApB));
            }
            d2=p*q;
            h11+=deci[i]*deci[i]*d2;
            h22+=d2;
            h21+=deci[i]*d2;
            d1=t[i]-p;
            g1+=deci[i]*d1;
            g2+=d1;
        }

        // Stopping Criteria
        if (fabs(g1)<eps && fabs(g2)<eps)
            break;

        // Finding Newton direction: -inv(H') * g
        det=h11*h22-h21*h21;
        dA=-(h22*g1 - h21 * g2) / det;
        dB=-(-h21*g1+ h11 * g2) / det;
        gd=g1*dA+g2*dB;

        stepsize = 1;		// Line Search
        while (stepsize >= minstep)
        {
            newA = A + stepsize * dA;
            newB = B + stepsize * dB;

            // New function value
            newf = 0.0;
            for (int i=0;i<len;i++)
            {
                fApB = deci[i]*newA+newB;
                if (fApB >= 0)
                    newf += t[i]*fApB + log(1+exp(-fApB));
                else
                    newf += (t[i] - 1)*fApB +log(1+exp(fApB));
            }
            // Check sufficient decrease
            if (newf<fval+0.0001*stepsize*gd)
            {
                A=newA;B=newB;fval=newf;
                break;
            }
            else
                stepsize = stepsize / 2.0;
        }

        if (stepsize < minstep)
        {
            printf("Line search fails in two-class probability estimates\n");
            break;
        }
    }

    if (it >= maxiter)
        printf("Reaching maximal iterations in two-class probability estimates\n");

    cout << "A = " << A << " B = " << B << endl;

    mSigmoidFunction[cate].first = A;
    mSigmoidFunction[cate].second = B;
}

/**
 * @brief package buildVocab and trainClassifier, handle training options
 */
void SVMCategorizer::trainModel()
{
    if (gPROG_ARGUMENT.count("model") || gPROG_ARGUMENT.count("vocabulary"))
    {
        directory_iterator iter(trainFolder), endIter;

        //search in template folder to organize the object templates by category
        for (; iter != endIter; ++iter)
        {
            if(is_directory(iter->path()))
            {
                string category = iter->path().filename().string();
                categoryNames.push_back(category);

                //create svm classifier
                svms[category] = ml::SVM::create();
                svms[category]->setType(ml::SVM::C_SVC);		//for multi-class svm
                svms[category]->setKernel(ml::SVM::INTER);		//histogram intersection kernel
                svms[category]->setC(1);
            }
        }

        //organize training images by category
        makeTrainingSet();

        //number of categories
        categories = categoryNames.size();
        cout << "Discovered " << categories << " object classes: ";

        //For debug
        for (int i = 0; i < categories; i++)
        {
            cout << categoryNames[i];
            if (i < categories - 1)
            {
                cout << ", ";
            }
        }
        cout << endl;

        //re-build vocabulary would need to re-build svm models
        if(gPROG_ARGUMENT.count("vocabulary"))
        {
            cout << "Re-train vocab and model.\n";
            buildVocab();
            trainClassifier();
        }
        //re-build svm models only
        else if (gPROG_ARGUMENT.count("model"))
        {
            cout << "Re-train model only.\n";
            FileStorage fs ("vocab.xml", FileStorage::READ);
            fs["vocabulary"] >> vocabulary;

            //set the vocabulary for the BOW descriptor extractor
            bowDescriptorExtractor->setVocabulary(vocabulary);
            trainClassifier();
        }
    }

    //load and use trained models (bag of words model and svm models)
    else
    {
        cout << "Re-train nothing.\n";
        loadSVMModel(this->modelFolder);
        loadSigmoid();
    }
}

/**
 * @brief Classify + localize objegct for saved images
 * @param testFolder path to test folder
 * @param numOfRegions number of regions to apply object classifier
 */
void SVMCategorizer::startStaticDetecting(const string &testFolder, int numOfRegions)
{
    cout << "Start detect with " << numOfRegions << endl;

    string testResStr = current_path().string() + string("/test_result");
    path testResPath(testResStr);

    if(!exists(testResPath))
    {
        if(!create_directory(testResPath))
        {
            cout << "Error create test_result directory.\n";
        }
    }

    createDirectories(testResPath.string(), categoryNames);

    directory_iterator endIter;

    //search for all image in test folder to classify with svm
    for(directory_iterator iter(testFolder); iter != endIter; ++ iter)
    {
        if(!is_directory(iter->path()))
        {
            vector<Rect> noUse;
            Mat readImg, frame, gFrame;
            string imgFileName;
            imgFileName = testFolder + string("/") + iter->path().filename().string();
            readImg = imread(imgFileName);
            resize(readImg, readImg, Size(640, 480));

            cout << "Read image: " << iter->path().filename().string() << endl;

            frame = readImg;
            frame = getROI(readImg).clone();

            pair<string, Mat> result = runDetection(frame, numOfRegions, noUse);

            string predictedCategory = result.first;
            Mat rframe = (result.second).clone();
            //draw out final result
            imwrite(testResPath.string() + string("/") + predictedCategory + string("/")
                    + iter->path().filename().string(), rframe);
        }

    }
}

/**
 * @brief overlapping bounding boxes suppression
 * @param boxes list of boxes score object positive
 * @param threshold	overlapping area threshold
 * @return list of picked boxes
 */
vector<Rect> SVMCategorizer::boxNMS(const vector<Rect> &boxes, double threshold)
{
    //index of boxes that will not eliminated
    vector<pair<int, int> > picked;

    //boxes that will not eliminated
    vector<Rect> pickedBox;

    //no box
    if(boxes.size() == 0)
    {
        return pickedBox;
    }

    //y2 coordinate
    vector<int> y2;

    //get the y2 coordinate
    for(int i = 0; i < boxes.size(); i++)
    {
        y2.push_back(boxes[i].y + boxes[i].height - 1);
    }

    //sort the box index according to the y2 value
    vector<int> indices = argSort(y2);

    //loop through the boxes until the index list - indices is empty
    while(indices.size() > 0)
    {
        //largest index
        int last = indices.size() - 1;
        int idx = indices[last];

        //add to picked list - will not be eliminated
        //first int - index, second int - number of overlapping boxes of it
        picked.push_back(pair<int, int>(idx, 0));

        //suppression list
        vector<int> suppress;

        //considering index will be eliminated after checking
        suppress.push_back(last);

        //loop through remain elements to calculate overlapping area
        for(int pos = last - 1; pos >= 0; pos--)
        {
            int j = indices[pos];

            //overlapping area greater than threshold, add to suppress list
            int res = checkOverlap(boxes[j], boxes[idx], threshold);
            if(res == 1)
            {
                suppress.push_back(pos);

                //increase number of overlapping box
                picked.back().second++;
            }

            //Considering box has a smaller box lie totally inside it
            //eliminate the considering box only
            else if(res == -1)
            {
                picked.pop_back();
                suppress.erase(suppress.begin() + 1, suppress.end());
                break;
            }
        }

        //eliminate elements in index list that are in suppress list
        for(int i = 0; i < suppress.size(); i++)
        {
            indices.erase(indices.begin() + suppress[i]);
        }
    }

    //base on index list, get picked boxes
    for(int i = 0; i < picked.size(); i++)
    {
        //just get box that has at least one overlapping box on it
        if(picked[i].second >= 1)
        {
            pickedBox.push_back(boxes[picked[i].first]);
        }
    }

    return pickedBox;
}

/**
 * @brief check if the overlapping area is greater than threshold
 * @param toCheckRect box will be checked
 * @param oriRect considering box
 * @param threshold	specific threshold - recommend 0.3 to 0.5
 * @return true if the overlapping area is greater than threshold
 */
int SVMCategorizer::checkOverlap(const Rect &toCheckRect, const Rect &oriRect, double threshold)
{
    //x1, y1 - top left
    //x2, y2 - bottom right
    int x1, y1, x2, y2;

    //get max x1, y1
    x1 = (toCheckRect.x > oriRect.x)? toCheckRect.x: oriRect.x;
    y1 = (toCheckRect.y > oriRect.y)? toCheckRect.y: oriRect.y;

    //coordinates of each rect
    int temp1x2 = (toCheckRect.x + toCheckRect.width - 1),
        temp1y2 = (toCheckRect.y + toCheckRect.height - 1),
        temp2x2 = (oriRect.x + oriRect.width - 1),
        temp2y2 = (oriRect.y + oriRect.height - 1);

    //get min x2, y2
    x2 = (temp1x2 < temp2x2)? temp1x2: temp2x2;
    y2 = (temp1y2 < temp2y2)? temp1y2: temp2y2;

    //overlap ratio = overlap area / original rect area
    double w = ((x2 - x1 + 1) > 0)? (x2 - x1 + 1): 0;
    double h = ((y2 - y1 + 1) > 0)? (y2 - y1 + 1): 0;
    double overlapRatio = (w*h) / oriRect.area();

    //ratio for small box totally inside the considering box
    //which is many time bigger than small box
    double overlapRatio2 = (w*h) / toCheckRect.area();

    //the ratio greater than threshold, box will be suppressed
    if(overlapRatio > threshold)
    {
        return 1;
    }

    if(overlapRatio2 > threshold)
    {
        return -1;
    }
    return 0;
}

/**
 * @brief load trained svm and bag of words models
 * @param pathToModelDir path to svm models directory
 */
void SVMCategorizer::loadSVMModel(const string &pathToModelDir)
{
    directory_iterator endIter;

    //load bag of words model
    FileStorage fs ("vocab.xml", FileStorage::READ);
    fs["vocabulary"] >> vocabulary;

    //set the vocabulary for the BOW descriptor extractor
    bowDescriptorExtractor->setVocabulary(vocabulary);

    //find all svm model files and load into svm object
    for(directory_iterator iter(pathToModelDir); iter != endIter; ++iter)
    {
        if(!is_directory(iter->path()))
        {
            string fullPath = pathToModelDir + string("/") + iter->path().filename().string();
            string fileName = iter->path().filename().string();

            cout << "Detect file: " << fileName << endl;

            int fIndex = fileName.find_last_of("SVM.");

            //get the category name
            string category = fileName.substr(0, fIndex - 3);

            //load into svm object
            svms[category] = ml::SVM::load(fullPath);

            //add loaded category name to category list
            categoryNames.push_back(category);
        }
    }

    //get the number of classes loaded into svm objects
    categories = categoryNames.size();
    cout << "Discovered " << categories << " object classes: ";

    //For debug
    for(int i = 0; i < categories; i++)
    {
        cout << categoryNames[i];
        if(i < categories - 1)
        {
            cout << ", ";
        }
    }
    cout << endl;
}

/**
 * @brief transforms size of object proposals bounding boxes from
 * 320x240 frame to 640x480 frame
 * @param oriRects 320x240 frame bounding boxes
 * @param ratio raito of widths of two frames
 * @return bounding boxes fit 640x480 frame
 */
vector<Rect> SVMCategorizer::transformRect(const vector<Rect> &oriRects, double ratio)
{
    vector<Rect> transformed(oriRects.size());
    for(int i = 0; i < oriRects.size(); i++)
    {
        transformed[i].x = (int)((double)oriRects[i].x * ratio);
        transformed[i].y = (int)((double)oriRects[i].y * ratio);
        transformed[i].width = (int)((double)oriRects[i].width * ratio);
        transformed[i].height = (int)((double)oriRects[i].height * ratio);
    }

    return transformed;
}

/**
 * @brief get re-define ROI of image (bottom 2/3 image)
 * @param img original image
 * @return sub-image
 */
Mat SVMCategorizer::getROI(const Mat &img)
{
    Rect rect(0, img.rows/3, img.cols, 2*img.rows/3);
    return img(rect).clone();
}

/**
 * @brief inherits from BaseObjectDetector, calls startDetecting(int numOfRegions)
 */
void SVMCategorizer::startDetecting()
{
    startDetecting(400);
}

/**
 * @brief run object detection on input each frame
 * @param frame raw frame to run object detection
 * @param numOfRegions number of regions to apply object classifier
 * @return successful or failed detection and processed frame (if successful)
 */
pair<string, Mat> SVMCategorizer::runDetection(Mat frame, int numOfRegions, vector<Rect> &rBoxes)
{
    boost::posix_time::time_duration duration;
    string detectedResult;
    Mat resultFrame;

    Mat frameForSss, gFrame;
    resize(frame, frameForSss, Size(frame.cols/2, frame.rows/2));

    cvtColor(frame, gFrame, CV_BGR2GRAY);

    //Selective search
    vector<Rect> rects;

    boost::posix_time::ptime t1 = boost::posix_time::microsec_clock::local_time();

    sssObj->setBaseImage(frameForSss);
    sssObj->switchToSelectiveSearchFast();
    sssObj->process(rects);

    rects = transformRect(rects, (frame.cols) / (frameForSss.cols));

    boost::posix_time::ptime t2 = boost::posix_time::microsec_clock::local_time();
    duration = t2 - t1;

//    cout << "Time for Selective Search: " << duration.total_milliseconds() << endl;

    string cate = "";

    set<pair<double, vector<int> >, comparator> scores;

    boost::posix_time::ptime t3 = boost::posix_time::microsec_clock::local_time();
    duration = t3 -t2;

//    cout << "Time predict whole image: " << duration.total_milliseconds() << endl;

    for(int id = 0; id < rects.size(); id++)
    {
        //reduce number of computed rectangles to numOfRegions
        if(id > numOfRegions) break;

        Mat subImg = gFrame(rects[id]).clone();		//sub frame for detect keypoint

        //eliminate the case whole image as bounding boxes
        //eliminate too small rectangles to reduce computation
        if((subImg.rows * subImg.cols >= 0.9 * gFrame.rows *gFrame.cols)
                || (subImg.rows * subImg.cols < 0.01 * gFrame.rows *gFrame.cols))
        {
            continue;
        }

        //keypoints and keypoint descriptors for each proposal
        vector<KeyPoint> kp;
        Mat testDesc;

        try
        {
            featureDetector->detect(subImg, kp);
        } catch (Exception &e)
        {
            cout << e.what() << endl;
        }


        bowDescriptorExtractor->compute(subImg, kp, testDesc);

        for(int i = 0; i < categories; i++)
        {
            //apply classifier to each rectangle
            try
            {
                //extract frame BOW descriptor
                cate = categoryNames[i];

                //get classifier score
                double raw = svms[cate]->predict(testDesc, noArray(), true);

                if(raw >= 0.0)
                {
                    continue;
                }


                double prob = 1.0 / (1.0 + exp(mSigmoidFunction[cate].first * raw
                                                   + mSigmoidFunction[cate].second));
                cout << "prob = " << prob << endl;

                if(prob > 0.5)
                {
                    Rect rect = rects[id];

                    //store proposals coordinates
                    vector<int> bbCoors;

                    //proposed bounding box coordinates
                    bbCoors.push_back(rect.x);
                    bbCoors.push_back(rect.y);
                    bbCoors.push_back(rect.width);
                    bbCoors.push_back(rect.height);

                    //add max perimeter of contour into map
                    pair<double, vector<int> > p1(prob, bbCoors);
                    scores.insert(p1);

                }
            }

            catch(Exception &ex)
            {
                continue;
            }

            break;
        }

    }

    boost::posix_time::ptime t4 = boost::posix_time::microsec_clock::local_time();
    duration = t4 - t3;

//    cout << "Time localize bounding boxes: " << duration.total_milliseconds() << endl;



    //no box has object positive
    if(scores.size() <= 0)
    {
        pair<string, Mat> p("", frame);
        cout << endl;
        return p;
    }

    //all boxes that score object positive
    vector<Rect> pBoxes;
    int k = 0;
    //translate separate coordinates into cv::Rect type
    for (auto setIter = scores.begin(); setIter != scores.end() && k < 15; ++setIter, ++k)
    {
        Rect rect(setIter->second[0], setIter->second[1], setIter->second[2], setIter->second[3]);
        pBoxes.push_back(rect);
    }

    //apply overlapping bounding boxes suppression
    rBoxes = findMaxOverlappedBox(pBoxes);

    boost::posix_time::ptime t5 = boost::posix_time::microsec_clock::local_time();
    duration = t5 -t4;

//    cout << "Time suppress overlapping boxes: " << duration.total_milliseconds() << endl << endl;

//    char str[255];
//    sprintf(str, "Prob %lf", scores.begin()->first);
//    putText(frame, str, Point(20,20), CV_FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0), 2, LINE_AA);
    pair<string, Mat> p(cate, frame);
    return p;
}

/**
 * @brief real-time object detection for frame from camera
 */
void SVMCategorizer::startDetecting(int numOfRegions)
{
    Mat frame = this->cameraController->getNewFrame();
    vector<Rect> rBoxes;

    frame = getROI(frame);

    //run detection on the image
    pair<string, Mat> result = runDetection(frame, numOfRegions, rBoxes);

    //get object name (e.g afc) and result frame
    string predictedCategory = result.first;
    Mat resultFrame = (result.second);

    cout << rBoxes.size() << " boxes.\n";

    Point2f point;

    //if there is bounding boxes detected, put annotations to image (coordinates of mid point)
    if(rBoxes.size() > 0)
    {
        //if there is more than one bounding boxes, follows the first one
        point = Point2f((2*rBoxes[0].x + rBoxes[0].width) / 2, (2*rBoxes[0].y + rBoxes[0].height) / 2);
        char outText[100];
        sprintf(outText, "Midpoint: (%lf,%lf)", point.x, point.y);

        //put coordinates of midpoint
        putText(resultFrame, outText, Point(10, 10), CV_FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0), 2, LINE_AA);

        //draw midpoint
        line(resultFrame, point, point, Scalar(255, 0, 0), 12);

        static int i = 0;
        imwrite(to_string(i++) + string(".jpg"),frame);
    }

    //no object detected
    else
    {
        point = Point2f(-1.0, -1.0);
    }

    cout << "Coordinates: " << point << endl;

    //update midpoint to send to pi1 and save predicted result
    updateMidPoint(point);

}
void SVMCategorizer::startVideoDetecting(const std::string &videoPath, int numOfRegions)
{
    VideoCapture cap(videoPath);

    Mat frame;
    while(cap.read(frame))
    {
        vector<Rect> rBoxes;

        frame = getROI(frame);

        //run detection on the image
        pair<string, Mat> result = runDetection(frame, numOfRegions, rBoxes);

        //get object name (e.g afc) and result frame
        string predictedCategory = result.first;
        Mat resultFrame = (result.second);

        cout << rBoxes.size() << " boxes.\n";

        Point2f point;

        //if there is bounding boxes detected, put annotations to image (coordinates of mid point)
        if(rBoxes.size() > 0)
        {
            //if there is more than one bounding boxes, follows the first one
            point = Point2f((2*rBoxes[0].x + rBoxes[0].width) / 2, (2*rBoxes[0].y + rBoxes[0].height) / 2);
            char outText[100];
            sprintf(outText, "Midpoint: (%lf,%lf)", point.x, point.y);

            //put coordinates of midpoint
            putText(resultFrame, outText, Point(10, 10), CV_FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0), 2, LINE_AA);

            //draw midpoint
            line(resultFrame, point, point, Scalar(255, 0, 0), 12);

            static int i = 0;
            imwrite(to_string(i++) + ".jpg",frame);
        }

            //no object detected
        else
        {
            point = Point2f(-1.0, -1.0);
        }

        cout << "Coordinates: " << point << endl;
    }
}


void SVMCategorizer::trackingVideo()
{
    //Video to track
    VideoCapture cap(gPROG_ARGUMENT["video"].as<string>());

    //output file for tracking result - should be .avi extension
    VideoWriter writer(gPROG_ARGUMENT["output"].as<string>(), VideoWriter::fourcc('X', 'V', 'I', 'D'), 10, Size(640, 320));

    Mat frame;

    //loop until the end of video
    while(cap.read(frame))
    {
        //bounding boxes returned from applying object detection on whole image
        vector<Rect> boxes;

        frame = getROI(frame);

        //detection result
        cout << "Run detection on frame.\n";
        pair<string, Mat> p = runDetection(frame, 400, boxes);

        //category name of detected object
        string category = p.first;

        //box to intialize tracker
        Rect2d box;

        //al least bounding box returned with object name is non-background
        if((p.first.compare("") != 0) && (p.first.compare("background") != 0) && (boxes.size() != 0))
        {
            //get the first box
            box = (Rect2d)boxes[0];

            //write to output video
            writer.write(frame);

            //intialize tracker
            cout << "Initialize tracker.\n";

            tracker.release();
            tracker = TrackerTLD::createTracker();
            tracker->init(frame, box);

            Point2f point = Point2f((2*box.x + box.width) / 2, (2*box.y + box.height) / 2);
            rectangle(frame, box, Scalar(0, 255, 0), 2, 1);

            //draw mid-point
            line(frame, point, point, Scalar(255, 0 ,0), 12);

            int numOfFail = 0;

            //contiunue to get frame from video for tracking
            while(cap.read(frame))
            {
                //number of continuously failed frames


                frame = getROI(frame);

                //time to get fps
                double timer = (double)getTickCount();

                //update tracking result
                cout << "Update tracking frame.\n";
                bool trackingResult = tracker->update(frame, box);

                //failure on tracking
                if(trackingResult == false)
                {
                    cout << "Failure on tracker.\n";
                    break;
                }

                //invalid box returned from tracking
                if(box.x < 0 || box.y < 0 || box.width < 0 || box.height < 0 ||
                        (box.x + box.width) > frame.cols || (box.y + box.height) > frame.rows)
                {
                    cout << "invalid box\n";
                    continue;
                }

                //get sub-image within returned bounding box from tracking
                Mat subFrame (frame((Rect)box));

                //convert to gray image
                cvtColor(subFrame, subFrame, CV_BGR2GRAY);

                //keypoints and keypoint descriptor of sub-image
                vector<KeyPoint> kp;
                Mat testDesc;

                double predict = 1.0;

                //detect feature
                featureDetector->detect(subFrame, kp);
                bowDescriptorExtractor->compute(subFrame, kp, testDesc);

                double prob = 0;

                //apply object detection on sub-image
                for(int i = 0; i < categories; i++)
                {
                    try
                    {
                        predict = svms[categoryNames[i]]->predict(testDesc, noArray(), true);

                        prob = 1.0 / (1.0 + exp(mSigmoidFunction[categoryNames[i]].first * predict
                                                           + mSigmoidFunction[categoryNames[i]].second));
                        cout << "prob. of tracked frame = " << prob << endl;

                        if(predict < 0)
                        {
                            category = categoryNames[i];

                            //reset number of continuous failed frames
                            numOfFail = 0;
                            break;

                        }
                    }
                    catch(Exception &ex)
                    {
                        break;
                    }

                }

                //non-object detected, increase number of continuous failed frame
                if(predict > 0)
                {
                    numOfFail++;

                    //number of continuous failed frame is greater than 5
                    //re-run detection on whole frame
                    if(numOfFail > 10)
                    {
                        cout << "Verification fails. Re-run detection.\n";
                        break;
                    }
                }

                //fps value
                float fps = getTickFrequency() / ((double)getTickCount() - timer);

                //put annotation on detected frame
                //draw bounding box
                if (predict < 0)
                    rectangle(frame, box, Scalar(0, 255, 0), 2, 1);
                else rectangle(frame, box, Scalar(0, 0, 255), 2, 1);

                //calculate mid-point coordinate
                Point2f point = Point2f((2*box.x + box.width) / 2, (2*box.y + box.height) / 2);
                char fpsStr[20];
                sprintf(fpsStr, "FPS: (%f)", fps);

                //draw fps value
                putText(frame, fpsStr, Point(100,50), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50,170,50), 2);

                char probStr[20];
                sprintf(probStr, "prob: %lf", prob);
                //draw category name of object
                putText(frame, probStr, Point(10,20), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,255,0), 2);


                //draw mid-point
                line(frame, point, point, Scalar(255, 0 ,0), 12);

                //write to output video
                writer.write(frame);
            }
        }

        //nothing detected, also write to output video
        else
        {
            writer.write(frame);
        }
    }
}

void SVMCategorizer::loadSigmoid()
{
    std::ifstream ifs("sigmoid");

    if (ifs.good())
    {
        boost::archive::text_iarchive ia(ifs);
        ia >> mSigmoidFunction;
        cout << mSigmoidFunction.begin()->second.first << endl;
    }
    else
    {
        char msg[100];
        sprintf(msg, "%s does not exist\n\n", "sigmoid");
        dieWithError(msg);
    }
}

void SVMCategorizer::saveSigmoid()
{
    std::ofstream ofs("sigmoid");
    boost::archive::text_oarchive oa(ofs);

    oa << mSigmoidFunction;
}

#ifdef CROSSCOMPILE
void SVMCategorizer::trackingCamera(bool storeVideo)
{
    //output file for tracking result - should be .avi extension
    VideoWriter writer("output.avi", VideoWriter::fourcc('X', 'V', 'I', 'D'), 5, Size(640, 320));

    Mat frame;

    //loop until the end of video
    while(true)
    {
        //bounding boxes returned from applying object detection on whole image
        vector<Rect> boxes;

        frame = cameraController->getNewFrame();
        frame = getROI(frame);

        //detection result
        cout << "Run detection on frame.\n";
        pair<string, Mat> p = runDetection(frame, 400, boxes);

        //category name of detected object
        string category = p.first;

        //box to intialize tracker
        Rect2d box;

        //al least bounding box returned with object name is non-background
        if((p.first.compare("") != 0) && (p.first.compare("background") != 0) && (boxes.size() != 0))
        {
            //get the first box
            box = (Rect2d)boxes[0];

            //intialize tracker
            cout << "Initialize tracker.\n";

            tracker.release();
            tracker = TrackerTLD::createTracker();
            tracker->init(frame, box);

            //contiunue to get frame from video for tracking
            while(true)
            {
                //number of continuously failed frames
                int numOfFail = 0;

                frame = cameraController->getNewFrame();
                frame = getROI(frame);

                //time to get fps
                double timer = (double)getTickCount();

                //update tracking result
                cout << "Update tracking frame.\n";
                bool trackingResult = tracker->update(frame, box);

                //failure on tracking
                if(trackingResult == false)
                {
                    cout << "Failure on tracker.\n";
                    break;
                }

                //invalid box returned from tracking
                if(box.x < 0 || box.y < 0 || box.width < 0 || box.height < 0 ||
                        (box.x + box.width) > frame.cols || (box.y + box.height) > frame.rows)
                {
                    cout << "invalid box\n";
                    continue;
                }

                //get sub-image within returned bounding box from tracking
                Mat subFrame (frame((Rect)box));

                //convert to gray image
                cvtColor(subFrame, subFrame, CV_BGR2GRAY);

                //keypoints and keypoint descriptor of sub-image
                vector<KeyPoint> kp;
                Mat testDesc;

                double predict = 1.0;

                //detect feature
                featureDetector->detect(subFrame, kp);
                bowDescriptorExtractor->compute(subFrame, kp, testDesc);

                double prob = 0;

                //apply object detection on sub-image
                for(int i = 0; i < categories; i++)
                {
                    try
                    {
                        predict = svms[categoryNames[i]]->predict(testDesc, noArray(), true);

                        prob = 1.0 / (1.0 + exp(mSigmoidFunction[categoryNames[i]].first * predict
                                                           + mSigmoidFunction[categoryNames[i]].second));
                        cout << "prob. of tracked frame = " << prob << endl;

                        if(predict < 0)
                        {
                            category = categoryNames[i];

                            //reset number of continuous failed frames
                            numOfFail = 0;
                            break;

                        }
                    }
                    catch(Exception &ex)
                    {
                        break;
                    }

                }

                //non-object detected, increase number of continuous failed frame
                if(predict > 0)
                {
                    numOfFail++;

                    //number of continuous failed frame is greater than 5
                    //re-run detection on whole frame
                    if(numOfFail > 10)
                    {
                        cout << "Verification fails. Re-run detection.\n";
                        break;
                    }
                }

                 //fps value
                float fps = getTickFrequency() / ((double)getTickCount() - timer);

                //put annotation on detected frame
                //draw bounding box
                if (predict < 0)
                    rectangle(frame, box, Scalar(0, 255, 0), 2, 1);
                else rectangle(frame, box, Scalar(0, 0, 255), 2, 1);

                //calculate mid-point coordinate
                Point2f point = Point2f((2*box.x + box.width) / 2, (2*box.y + box.height) / 2);
                char fpsStr[20];
                sprintf(fpsStr, "FPS: (%f)", fps);

                //draw fps value
                putText(frame, fpsStr, Point(100,50), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50,170,50), 2);

                char probStr[20];
                sprintf(probStr, "prob: %lf", prob);
                //draw category name of object
                putText(frame, probStr, Point(10,20), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,255,0), 2);


                //draw mid-point
                line(frame, point, point, Scalar(255, 0 ,0), 12);

                Json::Value response;
                response["x"] = point.x;
                response["y"] = point.y;

                Json::FastWriter jwriter;
                spokesmanPi2->send(jwriter.write(response).c_str());

                //write to output video
                if(storeVideo)
                {
                    writer.write(frame);
                }
            }
        }

        //nothing detected, also write to output video
        else
        {
            Json::Value response;
            response["x"] = -1;
            Json::FastWriter jwriter;
            spokesmanPi2->send(jwriter.write(response).c_str());

            if(storeVideo)
            {
                writer.write(frame);
            }
        }
    }
}
#else
void SVMCategorizer::trackingCamera(bool storeVideo){}
#endif

void SVMCategorizer::findOverlapBoxes(int index,
                                      const std::vector<cv::Rect> &bbox,
                                      list<int> &D,
                                      vector<list<Rect> > &T,
                                      list<Rect> &rectD,
                                      bool &valid)
{
    bool flag = true; // Check if the current box do not overlap any other boxes

    const Rect &curBox = rectD.back(); // get the current box from the list

    for (int j = index + 1; j < bbox.size(); j++)
    {
        if (checkOverlap(curBox, bbox[j], 0) == 1) // if two box overlapped
        {
            flag = false;

            // try
            D.push_back(j);
            rectD.push_back(overlapTwoBoxes(curBox, bbox[j]));

            if (trace[j]) valid = true; // valid for output if there is a non-traced box
            findOverlapBoxes(j, bbox, D, T, rectD, valid); // recursion

            // backtracking
            D.pop_back();
            rectD.pop_back();
        }
    }

    // if the current box do not overlap any others and there is a non-traced box in the list
    if (flag && valid)
    {
        T.push_back(rectD);
        for (auto i : D) trace[i] = false;
        valid = false;
    }

}

vector<Rect> SVMCategorizer::findMaxOverlappedBox(const std::vector<cv::Rect> &bbox)
{
    vector<list<Rect> > T; // List of overlapped box

    T.resize(0);
    memset(trace, true, 100);

    for (int i = 0; i < bbox.size(); i++)
    {
        list<int> D; // id of box
        list<Rect> rectD; // the box itself

        // add current box
        D.push_back(i);
        rectD.push_back(bbox[i]);

        bool valid = trace[i]; // valid for output
        findOverlapBoxes(i, bbox, D, T, rectD, valid);
    }

    vector<Rect> ret;

    // Store to the returning vector
    for (auto iter : T)
    {
        if (iter.size() > 0)
            ret.push_back(iter.back());
    }

    return ret;

}

cv::Rect SVMCategorizer::overlapTwoBoxes(const cv::Rect &firstBox, const cv::Rect &secondBox)
{
    int x1, y1, x2, y2;
    //get max x1, y1
    x1 = (firstBox.x > secondBox.x)? firstBox.x: secondBox.x;
    y1 = (firstBox.y > secondBox.y)? firstBox.y: secondBox.y;

    //coordinates of each rect
    int temp1x2 = (firstBox.x + firstBox.width),
        temp1y2 = (firstBox.y + firstBox.height),
        temp2x2 = (secondBox.x + secondBox.width),
        temp2y2 = (secondBox.y + secondBox.height);

    //get min x2, y2
    x2 = (temp1x2 < temp2x2)? temp1x2: temp2x2;
    y2 = (temp1y2 < temp2y2)? temp1y2: temp2y2;

    int w = x2 - x1;
    int h = y2 - y1;

    Rect ret;
    ret.x = x1;
    ret.y = y1;
    ret.width = w;
    ret.height = h;

    return ret;
}



