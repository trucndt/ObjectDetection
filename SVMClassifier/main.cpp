//
// Created by trucnguyen on 11/6/17.
//

#include "SVMCategorizer.h"

using namespace cv;
using namespace std;
using namespace boost::program_options;

#define SSTR( x ) static_cast< std::ostringstream & >( \
( std::ostringstream() << std::dec << x ) ).str()

variables_map gPROG_ARGUMENT;
int processCommandLineArgument(int argc, char **argv);
void tracker(CameraController *cam);

int main(int argc, char *argv[])
{
    if (processCommandLineArgument(argc, argv) < 0)
    {
        return 0;
    }

    cout << "Built time: " << __DATE__ << " " << __TIME__ << endl;

    SVMCategorizer *svm  = new SVMCategorizer(gPROG_ARGUMENT["clusters"].as<int>(), NULL,
                                               gPROG_ARGUMENT["train"].as<string>(), gPROG_ARGUMENT["svmfiles"].as<string>());
    svm->trainModel();

    if (gPROG_ARGUMENT.count("video"))
    {
    	if(gPROG_ARGUMENT.count("tracking"))
    	{
    		svm->trackingVideo();
    	}

    	else
    	{
    		svm->startVideoDetecting(gPROG_ARGUMENT["video"].as<string>(), gPROG_ARGUMENT["regions"].as<int>());
    	}
    }
    else
    {
        svm->startStaticDetecting(gPROG_ARGUMENT["test"].as<string>(), gPROG_ARGUMENT["regions"].as<int>());
    }

    return 0;
}

int processCommandLineArgument(int argc, char **argv)
{
    options_description usage("Usage");

    usage.add_options()
        ("help,h", "Print help message")
		("tracking", "Run tracking with detecting")
        ("video", value<string>() ,"Path to video for detecting/tracking")
		("output", value<string>() ,"Path to video for tracking result")
        ("vocabulary,v", "rebuild vocabulary")
        ("model,m", "rebuild SVM model")
        ("train,r", value<string>()->default_value("../data/train"), "Train directory")
        ("test,s", value<string>()->default_value("../data/test"), "Test directory")
        ("svmfiles,f", value<string>()->default_value("model"), "Model directory")
        ("clusters,c", value<int>()->default_value(300), "Number of words in the bag of words")
        ("regions,e", value<int>()->default_value(400), "Number of regions applied object classifier");

    try
    {
        store(command_line_parser(argc, argv).options(usage).run(), gPROG_ARGUMENT);

        if (gPROG_ARGUMENT.count("help"))
        {
            cout << usage << endl;
            return -1;
        }

        notify(gPROG_ARGUMENT);
    }
    catch (required_option& e)
    {
        cout << "ERROR: " << e.what() << endl;
        cout << usage << endl;
        return -1;
    }
    catch (boost::program_options::error &e)
    {
        cout << "ERROR: " << e.what() << endl;
        return -1;
    }

    return 0;
}

void tracker(CameraController *cam)
{
	Ptr<Tracker> tracker;
	tracker = Tracker::create("TLD");

	Mat frame;

	frame = cam->getNewFrame();
	Rect2d box = selectROI(frame, false);
	rectangle(frame, box, Scalar( 255, 0, 0 ), 2, 1 );
	imshow("Tracking", frame);
	tracker->init(frame, box);

	while(1)
	{
		frame = cam->getNewFrame();
		// Start timer
		double timer = (double)getTickCount();

		// Update the tracking result
		bool ok = tracker->update(frame, box);

		// Calculate Frames per second (FPS)
		float fps = getTickFrequency() / ((double)getTickCount() - timer);

		if (ok)
		{
			// Tracking success : Draw the tracked object
			rectangle(frame, box, Scalar( 255, 0, 0 ), 2, 1 );
		}
		else
		{
			// Tracking failure detected.
			putText(frame, "Tracking failure detected", Point(100,80), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,0,255),2);
		}

		// Display tracker type on frame
		putText(frame, "TLD Tracker", Point(100,20), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50,170,50),2);

		// Display FPS on frame
		putText(frame, "FPS : " + SSTR(int(fps)), Point(100,50), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50,170,50), 2);

		// Display frame.
		imshow("Tracking", frame);

		// Exit if ESC pressed.
		int k = waitKey(1);
		if(k == 'q')
		{
			break;
		}
	}
}

