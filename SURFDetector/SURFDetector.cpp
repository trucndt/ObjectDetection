/*
 * SURFDetector.cpp
 *
 *  Created on: Aug 25, 2017
 *      Author: baohq
 */

#include "SURFDetector.h"

using namespace std;
using namespace cv;
using namespace xfeatures2d;

class CameraController;

SURFDetector::SURFDetector()
{

}

SURFDetector::SURFDetector(CameraController *camCtrl, string imgspath) : BaseObjectDetector("SURF.csv")
{
    cameraControler = camCtrl;
    trainingImagePath = imgspath;
    featureDetector = SurfFeatureDetector::create();
    featureExtractor = SurfDescriptorExtractor::create();
}

void SURFDetector::trainModel()
{
    Mat trainingRgbImg = imread(trainingImagePath), trainingGrayImg;

    //create gray color image
    cvtColor(trainingRgbImg, trainingGrayImg, CV_RGB2GRAY);

    //detect key-points of gray color training image
    featureDetector->detect(trainingGrayImg, trainKp);

    //compute descriptor of gray color training image
    featureExtractor->compute(trainingGrayImg, trainKp, trainDesc);

    vector<Mat> trainDescs(1, trainDesc);

    //add and train descriptor of key-points of training image to FLANN matcher

    flannMatcher.add(trainDescs);
    flannMatcher.train();
}

//void SURFDetector::startFLANNDetect()
//{
//    //train key-points descriptor from training image
//    trainDescriptors();
//
//    //get image from camera and detect matches
//    while(char(waitKey(1)) != 'q')
//    {
//        double t0 = getTickCount();		//for calculate frame rate
//        Mat testRgbImg, testGImg;		//test image
//
//        //get new image frame from camera
//        testRgbImg = cameraControler->getNewFrame();
//        cvtColor(testRgbImg, testGImg, CV_RGB2GRAY);
//        vector<KeyPoint> testKp;		//test image key-points
//        Mat testDesc;					//test image kp descriptor
//
//        //detect key-points of test image
//        featureDetector->detect(testGImg, testKp);
//
//        //compute kp descriptor of test image
//        featureExtractor->compute(testGImg, testKp, testDesc);
//
//        //matches key-points between test image and train image
//        vector< vector<DMatch> > matches;
//
//        //find matching points
//        flannMatcher.knnMatch(testDesc, matches, 2);
//
//        //filter for good matches
//        vector<DMatch> goodMatches;
//        for(int i = 0; i < matches.size(); i++)
//        {
//            if(matches[i][0].distance < 0.6 * matches[i][1].distance)
//            {
//                goodMatches.push_back(matches[i][0]);
//            }
//        }
//
//        /* For debugging only
//        //draw result and show
//        Mat imgShow, trainImg = imread(trainingImagePath);
//        drawMatches(testRgbImg, testKp, trainImg, trainKp, goodMatches, imgShow);
//        imshow("result", imgShow);
//
//        cout << "Frame rate = " << getTickFrequency() / (getTickCount() - t0) << endl;
//         */
//        mCollect->recordFps(getTickFrequency() / (getTickCount() - t0));
//        mCollect->recordMatches(goodMatches.size());
//        if (mCollect->enoughTimes()) break;
//    }
//}


void SURFDetector::startDetecting()
{
    //train key-points descriptor from training image
    trainModel();

    //get image from camera and detect matches
    while(char(waitKey(1)) != 'q')
    {
        double t0 = getTickCount();		//for calculate frame rate
        Mat testRgbImg, testGImg;		//test image

        //get new image frame from camera
        testRgbImg = cameraControler->getNewFrame();
//        resize(testRgbImg, testRgbImg, Size(640, 480));
        cvtColor(testRgbImg, testGImg, CV_RGB2GRAY);
        vector<KeyPoint> testKp;		//test image key-points
        Mat testDesc;					//test image kp descriptor

        //detect key-points of test image
        featureDetector->detect(testGImg, testKp);

        //compute kp descriptor of test image
        featureExtractor->compute(testGImg, testKp, testDesc);

        //matches key-points between test image and train image
        vector<vector<DMatch> > matches;

        //find matching points
        flannMatcher.knnMatch(testDesc, matches, 2);

        //filter for good matches
        vector<DMatch> goodMatches;
        vector<KeyPoint> matched1, matched2;
        vector<Point2f> m1, m2;
        for(int i = 0; i < matches.size(); i++)
        {
            if(matches[i][0].distance < 0.6 * matches[i][1].distance)
            {
                goodMatches.push_back(matches[i][0]);
                matched1.push_back(trainKp[matches[i][0].trainIdx]);
                m1.push_back(trainKp[matches[i][0].trainIdx].pt);
                matched2.push_back(testKp[matches[i][0].queryIdx]);
                m2.push_back(testKp[matches[i][0].queryIdx].pt);
            }
        }

        Mat inlierMask, homography;
        vector<KeyPoint> inliers1, inliers2;
        vector<DMatch> inlierMatches;

        if (matched1.size() >= 4)
        {
            homography = findHomography(m1, m2, RANSAC, 2.5f, inlierMask);
//            homography = getPerspectiveTransform(m1, m2);
        }

        if (matched1.size() < 4 || homography.empty())
        {
            continue;
        }

        for (unsigned i = 0; i < matched1.size(); i++)
        {
            if (inlierMask.at<uchar>(i))
            {
                int newI = static_cast<int>(inliers1.size());
                inliers1.push_back(matched1[i]);
                inliers2.push_back(matched2[i]);
                inlierMatches.push_back(DMatch(newI, newI, 0));
            }
        }

        cout << "Frame rate = " << getTickFrequency() / (getTickCount() - t0) << endl;
        cout << "Matches = " << inlierMatches.size() << endl;

        Mat imgShow, trainingRgbImg = imread(trainingImagePath);;
        drawMatches(trainingRgbImg, inliers1, testRgbImg, inliers2, inlierMatches, imgShow);
//        drawMatches(testRgbImg, testKp, trainingRgbImg, trainKp, goodMatches, imgShow);
        //-- Get the corners from the image_1 ( the object to be "detected" )
        std::vector<Point2f> obj_corners(4);
        obj_corners[0] = cvPoint(0,0); obj_corners[1] = cvPoint(trainingRgbImg.cols, 0 );
        obj_corners[2] = cvPoint( trainingRgbImg.cols, trainingRgbImg.rows ); obj_corners[3] = cvPoint( 0, trainingRgbImg.rows );
        std::vector<Point2f> scene_corners(4);

        perspectiveTransform( obj_corners, scene_corners, homography);

        //-- Draw lines between the corners (the mapped object in the scene - image_2 )
        line( imgShow, scene_corners[0] + Point2f( trainingRgbImg.cols, 0), scene_corners[1] + Point2f( trainingRgbImg.cols, 0), Scalar(0, 255, 0), 4 );
        line( imgShow, scene_corners[1] + Point2f( trainingRgbImg.cols, 0), scene_corners[2] + Point2f( trainingRgbImg.cols, 0), Scalar( 0, 255, 0), 4 );
        line( imgShow, scene_corners[2] + Point2f( trainingRgbImg.cols, 0), scene_corners[3] + Point2f( trainingRgbImg.cols, 0), Scalar( 0, 255, 0), 4 );
        line( imgShow, scene_corners[3] + Point2f( trainingRgbImg.cols, 0), scene_corners[0] + Point2f( trainingRgbImg.cols, 0), Scalar( 0, 255, 0), 4 );

        resize(imgShow, imgShow, Size(1080, 720));
        imshow("result", imgShow);
    }
}

