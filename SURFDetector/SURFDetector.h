/*
 * SURFDetector.h
 *
 *  Created on: Aug 25, 2017
 *      Author: baohq
 */

#ifndef SURFDETECTOR_SURFDETECTOR_H_
#define SURFDETECTOR_SURFDETECTOR_H_

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include "../ObjectDetector/CameraController.h"
#include <boost/filesystem.hpp>
#include <ObjectDetector/BaseObjectDetector.h>

class SURFDetector : public BaseObjectDetector
{
public:
    SURFDetector();
    SURFDetector(CameraController *camCtrl, std::string imgspath);
    void trainModel();
    void startDetecting();
private:
    CameraController *cameraControler;	//Get image frame from camera
    std::string trainingImagePath;			//Path to training images used for training
    std::vector<cv::KeyPoint> trainKp;	//contains key-points of training image
    cv::Mat trainDesc;					//key-points descriptor of training image
    cv::Ptr<cv::xfeatures2d::SurfFeatureDetector> featureDetector;
    cv::Ptr<cv::xfeatures2d::SurfDescriptorExtractor> featureExtractor;
    cv::FlannBasedMatcher flannMatcher;
};

#endif /* SURFDETECTOR_SURFDETECTOR_H_ */
