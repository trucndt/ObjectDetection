//
// Created by trucndt on 5/12/17.
//

#include "ObjectTracker.h"

using namespace std;

ObjectTracker::ObjectTracker()
{

}

ObjectTracker::ObjectTracker(Mediator *aMediator,
                                 ServoController *aServoController,
                                 SpokesmanPi1 *aSpokesman)
{
    mMediator = aMediator;
    mServoController = aServoController;
    mSpokesman = aSpokesman;
}

void ObjectTracker::start()
{
    boost::thread thread(boost::bind(&ObjectTracker::run, this));
}

void ObjectTracker::run()
{
    Json::Value root;
    root["action"] = "ObjectDetector";

    sleep(1); //random sleep
    char buf[1024];

    while (true)
    {
        Json::FastWriter writer;

        int size;

        if ((size = mSpokesman->recv(buf)) < 0)
        {
            printf("Port_read error code: %d\n", size);
            usleep(100000);
            continue;
        }
        buf[size] = NULL;

        cout << "RECEIVED: " << buf << endl;

        receivedMessageHandler(buf, size);
        usleep(500000); // stabilizing the camera for anti-vibration frames

        // ACK -> finished processing
        mSpokesman->send("a", NULL);
    }
}

void ObjectTracker::receivedMessageHandler(const char *aMsg, int aMsgSize)
{
    Json::Reader reader;
    Json::Value root;

    if (reader.parse(aMsg, aMsg + aMsgSize, root, false) == false)
    {
        cout << "ERR: Cannot parse JSON" << endl;
        return;
    }

    if (root["x"].asDouble() < 0)
    {
        mServoController->stopRobot();
    }
    else
    {
        double OI = root["x"].asDouble() - 320;

        cout << "OI = " << OI << endl;

        double alpha = atan(OI/AO);

        cout << "alpha = " << alpha << " rad; degree = " << alpha * 180.0 / M_PI << endl;

        if (abs(alpha) < ANGLE_ERROR_THRESHOLD)
        {
            mServoController->setRpm(1000, -1000);
        }
        else
        {
            mServoController->turn(alpha, ROTATE_SPEED/5.0);
            usleep(alpha / (ROTATE_MAX_SPEED_RAD_S/5.0) * 1000000);
        }
    }
}
