/*
 * utils.h
 *
 *  Created on: Aug 18, 2016
 *      Author: trucndt
 */

#ifndef UTILS_H
#define UTILS_H
/* To avoid this error:
 *      undefined reference to `boost::system::generic_category()'
 */
#define BOOST_SYSTEM_NO_DEPRECATED

#include <iostream>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>

int makeRealTimeThread();
void dieWithError(const char *anErrMess);
std::string getIPAddress(const char *aDev);

/**
 * Convert vector of KeyPoints to vector of Point2f
 * @param anArrOfKey vector of KeyPoints
 * @return vector of Point2f
 */
std::vector<cv::Point2f> keyToPoints(const std::vector<cv::KeyPoint> &anArrOfKey);

/**
 * Compare two floating numbers, based on EPSILON
 * @param a 1st number
 * @param b 2nd number
 * @return 0 if two numbers are close, -1 if a < b, 1 otherwise.
 */
int compareFloating(double a, float b);

/**
 * Calculate distance between two point
 * @param ptA 1st point
 * @param ptB 2nd point
 * @return distance
 */
double calcDistance(const cv::Point2f& ptA, const cv::Point2f& ptB);

#endif
