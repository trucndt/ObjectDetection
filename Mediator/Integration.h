/**
 * @file Integration.h
 * @brief  define functions and enum for integration.
 * Copyright (C) 2015 Toshiba Corporation. All rights reserved.
 */

#ifndef INTEGRATION_H
#define INTEGRATION_H

#include <signal.h>

/* Maximum detect count to search target execution */
#define MAX_DETECT_COUNT      5
/* Count of around search */
#define SEARCH_ROTATE_COUNT   1
/* Degree of rotate to search target */
#define SEARCH_ROTATE_DEGREE  360
/* Maximum distance to start moving */
#define MAX_DISTANCE          2000
/* Interval of time between detection
 * DETECT_INTERVAL * 5ms */
#define DETECT_INTERVAL       5

/* List of all hand style ID*/
typedef enum {
    ROCK = 0,
    SCISSORS,
    PAPER
}   DECISION;

/*define structure for Term in Triangle Shape*/
struct Triangle{
    double dLeft;
    double dMiddle;
    double dRight;
};

/*define structure for Term in Trapezoid Shape*/
struct Trapezoid{
    double dLeft;
    double dLeftMiddle;
    double dRightMiddle;
    double dRight;
};
/* Define the for integration application*/
void Integration_initialize(const char ** argv);
void Integration_start(void);

/* Define the function for timer*/
typedef void (*timer_handler)(union sigval);
void timer_initialize(timer_handler h);
void timer_start(void);
void timer_stop(void);
    
/* Define the function for support avoid obstacle*/
double Avoidance_dist_process(double d_dist);
double getValueTriangle(double t, struct Triangle Triangle1);
double getValueTrapezoid(double t, struct Trapezoid Trapezoid1);
double max(double a, double b);

#endif /* INTEGRATION_H */
