#include "SpokesmanPi1.h"
#include <string.h>

using namespace std;

SpokesmanPi1::SpokesmanPi1(Mediator* aMediator)
{
    mMediator = aMediator;
}

int SpokesmanPi1::initialize()
{
    /* Initializes UART ch2 and create Communication class instance */
    if (Port_initialize(UART_PORT) != COM_NO_ERROR)
    {
        printf("Communication_initialize error.\n");
        return -1;
    }
    Port_set_error_detection(UART_PORT, 1);

    return 0;
}

int SpokesmanPi1::send(const char *aMsg, char *aResponse)
{
    boost::lock_guard<boost::mutex> lockGuard(mMtx_Port);

    cout << "Sending to Pi2 ..." << endl;

    int ret = Port_write(UART_PORT, aMsg, strlen(aMsg));
    printf("Port_write return code: %d\n", ret);

    return ret;
}

int SpokesmanPi1::recv(char *aMsg)
{
    boost::lock_guard<boost::mutex> lockGuard(mMtx_Port);

    return Port_read(UART_PORT, aMsg, MAX_SIZE_READ);
}
