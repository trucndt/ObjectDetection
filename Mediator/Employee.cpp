/*
 * Employee.cpp
 *
 *  Created on: Feb 22, 2017
 *      Author: trucndt
 */

#include "Employee.h"

using namespace std;

Employee::Employee(Mediator* aMediator, SpokesmanPi1 *aSpokesman, ServoController *aServoController, int aSock)
{
    mMediator = aMediator;
    mSpokesman = aSpokesman;
    mSock = aSock;
    mServoController = aServoController;
}

void Employee::start()
{
    boost::thread clntThread(&Employee::clientHdl, this);
}

Employee::~Employee()
{

}

void Employee::clientHdl()
{
    char recvBuffer[RCV_BUFFER_SIZE];
    int recvMsgSize;

    /* Connection establishment */
    if (sessionInitiation() < 0) //failed to create a session
    {
        cout << "Cannot initiate session\n";
        close(mSock);
        delete this;
        return;
    }

    /* Commands sequence */
    while (true)
    {
        if ((recvMsgSize = recv(mSock, recvBuffer, RCV_BUFFER_SIZE, 0)) <= 0)
        {
            cout << "recv() failed from: " << __FILE__ << ":" << __LINE__ << endl;
            cout << "rcvMsgSize = " << recvMsgSize << endl;

            close(mSock);
            delete this;
            return;
        }
        recvBuffer[recvMsgSize] = NULL;
        cout << "Received " << recvMsgSize << ": " << recvBuffer << endl;

        receivedCmdHandler(recvBuffer, recvMsgSize);
    }

    return;
}

int Employee::sendToClient(const Json::Value &aRoot)
{
    string outMsg = aRoot.toStyledString();

    cout << "outMsg's size = " << outMsg.size() << endl;
    cout << "outMsg = " << outMsg << endl;

    /* rcsApp uses readline() */
    Json::FastWriter fWriter;
    outMsg = fWriter.write(aRoot);

    if (send(mSock, outMsg.c_str(), outMsg.size(), 0) != outMsg.size())
    {
        cout << "send() failed from: " << __FILE__ << ":" << __LINE__ << endl;
        return -1;
    }

    return 0;
}

int Employee::receivedCmdHandler(const char *aRcvMsg, int aRcvMsgSize)
{
    Json::Reader reader;
    Json::Value root;

    if (reader.parse(aRcvMsg, aRcvMsg + aRcvMsgSize, root) == 0)
    {
        cout << "Error: Parse JSON failed.\n";
    }

    if (root["action"] == "move")
    {
        moveServo(root);
    }
    else if (root["action"] == "getRobotInfo")
    {
        getRobotInfo(root);
    }
    else if (root["action"] == "getServoInfo")
    {
        getServoInfo(root);
    }
    else if (root["action"] == "stream")
    {
        getStreamingInfo(root);
    }
    else
    {
        cout << "ERR: Invalid command: " << aRcvMsg << endl;
        cmdNotSupported(root["action"].asString());
    }

}

int Employee::moveServo(const Json::Value &aRoot)
{
    Json::Value response, argument;
    int8_t ret = 0;

    response["result"] = "SUCCESS";
    response["description"] = "Move successfully";

    /* Check for errors */
    if (aRoot.isMember("leftMotor") == false || aRoot.isMember("rightMotor") == false)
    {
        ret = -1;
    }
    else
    {
        short right = aRoot["rightMotor"].asInt();
        short left = aRoot["leftMotor"].asInt();

        mServoController->setRpm(right, left);
    }

    if (ret == -1)
    {
        response["result"] = "FAILED";
        response["description"] = "Missing required parameters";
    }

//    Not needed
//    sendToClient(response);

    return ret;
}

void Employee::cmdNotSupported(const string& aCmd)
{
    Json::Value response;

    response["result"] = "FAILED";
    response["description"] = string("The command " + aCmd + " is not supported");

    sendToClient(response);
}

int Employee::sessionInitiation()
{
    char rcvBuffer[RCV_BUFFER_SIZE];
    int rcvMsgSize, ret;
    Json::Reader reader;
    Json::Value root, response;

    if ((rcvMsgSize = recv(mSock, rcvBuffer, RCV_BUFFER_SIZE, 0)) <= 0)
    {
        cout << "recv() failed from: " << __FILE__ << ":" << __LINE__ << endl;
        return -1;
    }
    rcvBuffer[rcvMsgSize] = 0;
    cout << "Received " << rcvMsgSize << ": " << rcvBuffer << endl;

    if (reader.parse(rcvBuffer, rcvBuffer + rcvMsgSize, root) == 0)
    {
        cout << "Error: Parse JSON failed.\n";
        return -1;
    }

    response["result"] = "SUCCESS";
    response["description"] = "Session Initiated";
    ret = 0;

    /* Check for errors */
    if (root.isMember("action") == false || root.isMember("pass") == false)
    {
        ret = -1;
    }
    else if (root["action"].asString() != "SessionInitiation")
    {
        ret = -2;
    }
    else
    {
        if (mMediator->comparePassword(root["pass"].asString()) == false)
        {
            ret = -3;
        }
    }

    /* Generate response */
    if (ret < 0)
    {
        response["result"] = "FAILED";
        if (ret == -1) response["description"] = "Missing required parameters";
        else if (ret == -2) response["description"] = "You must initiate a session first";
        else if (ret == -3) response["description"] = "Wrong password";
    }

    sendToClient(response);

    return ret;
}

int Employee::getRobotInfo(const Json::Value &aRoot)
{
    Json::Value response;

    response["result"] = "SUCCESS";
    response["description"] = "Get Robot info";
    response["value"] = mServoController->getOdometer();

    sendToClient(response);

    return 0;
}

int Employee::getServoInfo(const Json::Value &aRoot)
{
    Json::Value response;

    response["result"] = "SUCCESS";
    response["description"] = "Get Servo info";
    response["value"] = mServoController->getServoInfo();

    sendToClient(response);

    return 0;
}

int Employee::getStreamingInfo(const Json::Value &aRoot)
{
    int8_t ret = 0;
    Json::Value response;

    response["result"] = "SUCCESS";
    response["description"] = "Streaming";

    Json::Value requestIP;
    requestIP["action"] = "requestIP";
    Json::FastWriter writer;
    string outMsg = writer.write(requestIP);
    char buf[1024];

    int byteRcv = mSpokesman->send(writer.write(requestIP).c_str(), buf);
    requestIP.clear();

    Json::Reader reader;
    reader.parse(buf, buf + byteRcv, requestIP, false);

    response["IP"] = requestIP["IP"];
    response["port"] = requestIP["port"];

    sendToClient(response);

    return ret;
}
