/*
 * Employee.h
 *
 *  Created on: Feb 22, 2017
 *      Author: trucndt
 */

#ifndef MEDIATOR_EMPLOYEE_H_
#define MEDIATOR_EMPLOYEE_H_

#include "Mediator.h"
#include "SpokesmanPi1.h"
#include "ServoController.h"
#include "json/json.h"

class Mediator;
class SpokesmanPi1;
class ServoController;

class Employee
{
public:
    Employee(Mediator* aMediator, SpokesmanPi1 *aSpokesman, ServoController* aServoController, int aSock);

	void start();

	virtual ~Employee();

private:
    static const int RCV_BUFFER_SIZE = 1024;
	int mSock;
	Mediator *mMediator;
    SpokesmanPi1 *mSpokesman;
    ServoController *mServoController;

	void clientHdl();
    int sendToClient(const Json::Value& aRoot);
    int receivedCmdHandler(const char* aRcvMsg, int aRcvMsgSize);
    void cmdNotSupported(const std::string& aCmd);

    int moveServo(const Json::Value& aRoot);

	/**
	 * Handle Get Robot Info command
	 * @param aRoot The Json of the incoming command
	 * @return 0 if success
	 */
	int getRobotInfo(const Json::Value& aRoot);

	/**
	 * Handle Get Servo Info command
	 * @param aRoot The Json of the incoming command
	 * @return 0 if success
	 */
	int getServoInfo(const Json::Value& aRoot);

	/**
	 * Handle Stream command
	 * @param aRoot The Json of the incoming command
	 * @return 0 if success
	 */
	int getStreamingInfo(const Json::Value& aRoot);

    /**
     * @brief Handle session initiation command
     * @return 0 if success, negative otherwise.
     */
    int sessionInitiation();
};

#endif /* MEDIATOR_EMPLOYEE_H_ */
