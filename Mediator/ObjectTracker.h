//
// Created by trucndt on 5/12/17.
//

#ifndef RCSEMBEDDED_LANELINECONTROL_H
#define RCSEMBEDDED_LANELINECONTROL_H

#include "Mediator.h"

class Mediator;
class ServoController;

class ObjectTracker
{
public:
    ObjectTracker();
    ObjectTracker(Mediator *aMediator, ServoController *aServoController, SpokesmanPi1 *aSpokesman);
    void start();

private:
    static constexpr double ROTATE_ANGLE_RADIAN = 0.28396507;
    static constexpr double ROTATE_SPEED = 100;
    static constexpr double ROTATE_MAX_SPEED_RAD_S = 7.337;
    static constexpr double AO = 624.4;
    static constexpr double ANGLE_ERROR_THRESHOLD = 0.174533;

    Mediator* mMediator;
    ServoController* mServoController;
    SpokesmanPi1* mSpokesman;

    void run();

    /**
     * process messages received from Pi2
     * @param aMsg the message
     * @param aMsgSize size of the message
     */
    void receivedMessageHandler(const char *aMsg, int aMsgSize);
};


#endif //RCSEMBEDDED_LANELINECONTROL_H
