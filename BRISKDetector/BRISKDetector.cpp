//
// Created by trucnguyen on 9/24/17.
//

#include "BRISKDetector.h"

using namespace std;
using namespace cv;
using namespace xfeatures2d;

BRISKDetector::BRISKDetector()
{
}

BRISKDetector::BRISKDetector(CameraController *camCtrl, const char *imgpath) : BaseObjectDetector("BRISK.csv")
{
    cameraControler = camCtrl;
    trainingRgbImg = imread(imgpath);

    mBrisk = BRISK::create();
}

void BRISKDetector::trainModel()
{
    Mat trainingGrayImg;

    //create gray color image
    cvtColor(trainingRgbImg, trainingGrayImg, CV_RGB2GRAY);

    //detect key-points of gray color training image
    mBrisk->detect(trainingGrayImg, trainKp);

    //compute descriptor of gray color training image
    mBrisk->compute(trainingGrayImg, trainKp, trainDesc);

    flannIndex = new flann::Index(trainDesc, flann::LshIndexParams(12, 20, 2), cvflann::FLANN_DIST_HAMMING);
}

void BRISKDetector::startDetecting()
{
    mMidPoint.x = -1; mMidPoint.y = -1;

    double t0 = getTickCount();		//for calculate frame rate
    Mat testRgbImg, testGImg;		//test image

    //get new image frame from camera
    testRgbImg = cameraControler->getNewFrame();
    cvtColor(testRgbImg, testGImg, CV_RGB2GRAY);
    vector<KeyPoint> testKp;		//test image key-points
    Mat testDesc;					//test image kp descriptor

    //detect key-points of test image
    mBrisk->detect(testGImg, testKp);

    //compute kp descriptor of test image
    mBrisk->compute(testGImg, testKp, testDesc);

    // If cannot extract any descriptor, knnSearch will throw error
    if (testDesc.empty())
    {
        updateMidPoint(Point2f(-1, -1));
        return;
    }

    //find matching points
    Mat matchIdx(testDesc.rows, 2, CV_32SC1);
    Mat matchDist(testDesc.rows, 2, CV_32FC1);
    flannIndex->knnSearch(testDesc, matchIdx, matchDist, 2, flann::SearchParams());

    //filter for good matches
    vector<KeyPoint> matched1, matched2;
    vector<Point2f> m1, m2;
    for(int i = 0; i < matchDist.rows; i++)
    {
        if(matchDist.at<float>(i, 0) < 0.8 * matchDist.at<float>(i, 1))
        {
            DMatch dm(i, matchIdx.at<int>(i, 0), matchDist.at<float>(i, 0));
            matched1.push_back(trainKp[dm.trainIdx]);
            matched2.push_back(testKp[dm.queryIdx]);
        }
    }

    Mat inlierMask, homography;
    vector<KeyPoint> inliers1, inliers2;
    vector<DMatch> inlierMatches;

    if (matched1.size() >= 4)
    {
        // Find homography using RANSAC
        homography = findHomography(keyToPoints(matched1), keyToPoints(matched2), CV_RANSAC, 2.5f, inlierMask);
    }

    if (matched1.size() < 4 || homography.empty())
    {
        /* update mMidPoint */
        cout << "Frame rate = " << getTickFrequency() / (getTickCount() - t0) << endl;
        updateMidPoint(Point2f(-1, -1));
        return;
    }

//        /* For debugging purpose

    cout << "Frame rate = " << getTickFrequency() / (getTickCount() - t0) << endl;
    cout << "Matches = " << inlierMatches.size() << endl;

    // Uncomment the line below to draw good matches only
    //drawMatches(testRgbImg, testKp, trainingRgbImg, trainKp, goodMatches, imgShow);

    //-- Get the corners from the image_1 ( the object to be "detected" )
    std::vector<Point2f> obj_corners(4);
    obj_corners[0] = cvPoint(0, 0);
    obj_corners[1] = cvPoint(trainingRgbImg.cols, 0);
    obj_corners[2] = cvPoint(trainingRgbImg.cols, trainingRgbImg.rows);
    obj_corners[3] = cvPoint(0, trainingRgbImg.rows);

    std::vector<Point2f> scene_corners(4);

    perspectiveTransform(obj_corners, scene_corners, homography);

    //-- Draw lines between the corners (the mapped object in the scene - image_2 )
    line(testRgbImg, scene_corners[0], scene_corners[1], Scalar(0, 255, 0), 4);
    line(testRgbImg, scene_corners[1], scene_corners[2], Scalar(0, 255, 0), 4);
    line(testRgbImg, scene_corners[2], scene_corners[3], Scalar(0, 255, 0), 4);
    line(testRgbImg, scene_corners[3], scene_corners[0], Scalar(0, 255, 0), 4);

    /* update mMidPoint */
    updateMidPoint(goodBoundingBoxChecking(scene_corners));

    if (mMidPoint.x < 0)
    {
        cout << "Bad bounding box\n";
    }
    else
    {
        cout << "Mid point: " << mMidPoint << endl;
        line(testRgbImg, mMidPoint, mMidPoint, Scalar(255, 0, 0), 12);
    }

//    imshow("BRISK", testRgbImg);

//        */

    /* For collecting result, uncomment this block
    mCollect->recordFps(getTickFrequency() / (getTickCount() - t0));
    mCollect->recordMatches(inlierMatches.size());
    if (mCollect->enoughTimes()) break;
     */
}
