//
// Created by trucnguyen on 9/24/17.
//

#ifndef OBJECTDETECTION_BRISKDETECTOR_H
#define OBJECTDETECTION_BRISKDETECTOR_H

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <ObjectTracking/CameraController.h>
#include <ObjectTracking/BaseObjectDetector.h>

class BRISKDetector : public BaseObjectDetector
{
public:
    BRISKDetector();
    BRISKDetector(CameraController *camCtrl, const char *imgspath);

    void trainModel();
    void startDetecting();

private:
    CameraController *cameraControler;	//Get image frame from camera
    std::vector<cv::KeyPoint> trainKp;	//contains key-points of training image
    cv::Mat trainDesc;					//key-points descriptor of training image
    cv::Ptr<cv::BRISK> mBrisk;
    cv::flann::Index *flannIndex;
    cv::Mat trainingRgbImg;
};


#endif //OBJECTDETECTION_BRISKDETECTOR_H
