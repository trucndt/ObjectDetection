/*
 * ORBDetector.cpp
 *
 *  Created on: Aug 26, 2017
 *      Author: baohq
 */

#include "ORBDetector.h"

using namespace std;
using namespace cv;
using namespace xfeatures2d;

class CameraController;

ORBDetector::ORBDetector()
{

}

ORBDetector::ORBDetector(CameraController *camCtrl, string imgpath) : BaseObjectDetector("ORB.csv")
{
    cameraControler = camCtrl;
    trainingImagePath = imgpath;

    orbObj = ORB::create();
}

void ORBDetector::trainModel()
{
    Mat trainingGrayImg;

    //create gray color image
    Mat trainingRgbImg = imread(trainingImagePath);
    cvtColor(trainingRgbImg, trainingGrayImg, CV_RGB2GRAY);

    //detect key-points of gray color training image
    orbObj->detect(trainingGrayImg, trainKp);

    //compute descriptor of gray color training image
    orbObj->compute(trainingGrayImg, trainKp, trainDesc);
}

void ORBDetector::startDetecting()
{
    //train key-points descriptor from training image
    trainModel();

    flannIndex = new flann::Index(trainDesc, flann::LshIndexParams(12, 20, 2), cvflann::FLANN_DIST_HAMMING);

    //get image from camera and detect matches
    while(char(waitKey(1)) != 'q')
    {
        double t0 = getTickCount();		//for calculate frame rate
        Mat testRgbImg, testGImg;		//test image

        //get new image frame from camera
        testRgbImg = cameraControler->getNewFrame();
        cvtColor(testRgbImg, testGImg, CV_RGB2GRAY);
        vector<KeyPoint> testKp;		//test image key-points
        Mat testDesc;					//test image kp descriptor

        //detect key-points of test image
        orbObj->detect(testGImg, testKp);

        //compute kp descriptor of test image
        orbObj->compute(testGImg, testKp, testDesc);

        // If cannot extract any descriptor, knnSearch will throw error
        if (testDesc.empty())
        {
            continue;
        }

        //find matching points
        Mat matchIdx(testDesc.rows, 2, CV_32SC1);
        Mat matchDist(testDesc.rows, 2, CV_32FC1);
        flannIndex->knnSearch(testDesc, matchIdx, matchDist, 2, flann::SearchParams());

        //filter for good matches
        vector<DMatch> goodMatches;
        vector<KeyPoint> matched1, matched2;
        vector<Point2f> m1, m2;
        for(int i = 0; i < matchDist.rows; i++)
        {
            if(matchDist.at<float>(i, 0) < 0.8 * matchDist.at<float>(i, 1))
            {
                DMatch dm(i, matchIdx.at<int>(i, 0), matchDist.at<float>(i, 0));
                goodMatches.push_back(dm);
                matched1.push_back(trainKp[dm.trainIdx]);
                matched2.push_back(testKp[dm.queryIdx]);
            }
        }

        Mat inlierMask, homography;
        vector<KeyPoint> inliers1, inliers2;
        vector<DMatch> inlierMatches;

        if (matched1.size() >= 4)
        {
            // Find homography using RANSAC
            homography = findHomography(keyToPoints(matched1), keyToPoints(matched2), CV_RANSAC, 2.5f, inlierMask);
        }

        if (!(matched1.size() < 4 || homography.empty()))
        {
            // Get inliers
            for (int i = 0; i < matched1.size(); i++)
            {
                if (inlierMask.at<uchar>(i))
                {
                    int newI = static_cast<int>(inliers1.size());
                    inliers1.push_back(matched1[i]);
                    inliers2.push_back(matched2[i]);
                    inlierMatches.push_back(DMatch(newI, newI, 0));
                }
            }
        }

        /* For debugging purpose

        cout << "Frame rate = " << getTickFrequency() / (getTickCount() - t0) << endl;
        cout << "Matches = " << inlierMatches.size() << endl;

        Mat imgShow, trainingRgbImg = imread(trainingImagePath);
        drawMatches(trainingRgbImg, inliers1, testRgbImg, inliers2, inlierMatches, imgShow);

        // Uncomment the line below to draw good matches only
        //drawMatches(testRgbImg, testKp, trainingRgbImg, trainKp, goodMatches, imgShow);

        //-- Get the corners from the image_1 ( the object to be "detected" )
        std::vector<Point2f> obj_corners(4);
        obj_corners[0] = cvPoint(0, 0);
        obj_corners[1] = cvPoint(trainingRgbImg.cols, 0);
        obj_corners[2] = cvPoint(trainingRgbImg.cols, trainingRgbImg.rows);
        obj_corners[3] = cvPoint(0, trainingRgbImg.rows);

        std::vector<Point2f> scene_corners(4);

        perspectiveTransform(obj_corners, scene_corners, homography);

        //-- Draw lines between the corners (the mapped object in the scene - image_2 )
        Point2f offset( trainingRgbImg.cols, 0);
        line( imgShow, scene_corners[0] + offset, scene_corners[1] + offset, Scalar(0, 255, 0), 4 );
        line( imgShow, scene_corners[1] + offset, scene_corners[2] + offset, Scalar( 0, 255, 0), 4 );
        line( imgShow, scene_corners[2] + offset, scene_corners[3] + offset, Scalar( 0, 255, 0), 4 );
        line( imgShow, scene_corners[3] + offset, scene_corners[0] + offset, Scalar( 0, 255, 0), 4 );

        resize(imgShow, imgShow, Size(1080, 720));
        imshow("ORB", imgShow);

        */

        mCollect->recordFps(getTickFrequency() / (getTickCount() - t0));
        mCollect->recordMatches(inlierMatches.size());
        if (mCollect->enoughTimes()) break;
    }
}
