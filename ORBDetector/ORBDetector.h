/*
 * ORBDetector.h
 *
 *  Created on: Aug 26, 2017
 *      Author: baohq
 */

#ifndef ORBDETECTOR_ORBDETECTOR_H_
#define ORBDETECTOR_ORBDETECTOR_H_

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include "../ObjectDetector/CameraController.h"
#include <ObjectDetector/BaseObjectDetector.h>

class ORBDetector : public BaseObjectDetector
{
public:
    ORBDetector();
    ORBDetector(CameraController *camCtrl, std::string imgspath);
    void trainModel();
    void startDetecting();
private:
    CameraController *cameraControler;	//Get image frame from camera
    std::string trainingImagePath;			//Path to training images used for training
    std::vector<cv::KeyPoint> trainKp;	//contains key-points of training image
    cv::Mat trainDesc;					//key-points descriptor of training image
    cv::Ptr<cv::ORB> orbObj;
    cv::flann::Index *flannIndex;
};

#endif /* ORBDETECTOR_ORBDETECTOR_H_ */
